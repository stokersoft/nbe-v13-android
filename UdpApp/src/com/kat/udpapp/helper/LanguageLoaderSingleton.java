package com.kat.udpapp.helper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;

import com.kat.udpapp.Constants;
import com.kat.udpapp.R;
import com.kat.udpapp.SecurePreferences;
import com.kat.xmlprofile.helpers.ApplicationGlobles;

public class LanguageLoaderSingleton {

	private static LanguageLoaderSingleton languageLoaderSingleton = new LanguageLoaderSingleton();
	private static volatile LanguageLoaderSingleton instance = null;

	public static HashMap<String, String> LanguageHashMap = new HashMap<String, String>();

	private LanguageLoaderSingleton() {
	}

	public static void reset() {
		instance = new LanguageLoaderSingleton();

	}

	public static LanguageLoaderSingleton getInstance(Context context) {

		if (instance == null) {
			instance = new LanguageLoaderSingleton();

		}
		Loadlanguage(context);
		return instance;

	}

	private static void Loadlanguage(Context context) {
		LanguageHashMap.clear();
		BufferedReader reader = null;

		try {

			if (SecurePreferences.getStringPreference(context,
					Constants.prefLanguage).equals(
					Constants.languageNameEnglise)) { 

				reader = new BufferedReader(
						new InputStreamReader(context.getResources()
								.openRawResource(R.raw.lang_uk), "utf-8"));

			} else

			{
				reader = new BufferedReader(
						new InputStreamReader(context.getResources()
								.openRawResource(R.raw.lang_dk), "utf-8"));

			}

			String lines = "";
			try {
				while ((lines = reader.readLine()) != null) {

					String[] splittedLanguage = lines.split("=");
					LanguageHashMap.put(splittedLanguage[0],
							splittedLanguage[1]);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static String getStringFromLanguage(String stringId) {
		String returnedString = "";
		if (ApplicationGlobles.isNullOrEmpty(stringId)) {
			returnedString = "null";

		} else {
			if (ApplicationGlobles.isNullOrEmpty(LanguageHashMap.get(stringId))) {
				returnedString = stringId;
			} else {
				returnedString = LanguageHashMap.get(stringId);

			}

		}

		return returnedString;

	}

}
