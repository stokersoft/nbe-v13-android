/**
 * ORM for handling FieldMinMax
 * 
 * 02/06-2015 - stokersoft
 */
package com.kat.udpapp.helper;

import com.kat.models.FieldMinMax;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class FieldMinMaxORM {

    private static final String TABLE_NAME = "minmax";

    private static final String COMMA_SEP = ", ";

    private static final String COLUMN_NAME_TYPE = "TEXT PRIMARY KEY";
    private static final String COLUMN_NAME = "name";

    private static final String COLUMN_MIN_TYPE = "TEXT";
    private static final String COLUMN_MIN = "min";

    private static final String COLUMN_MAX_TYPE = "TEXT";
    private static final String COLUMN_MAX = "max";
    
    private static final String COLUMN_DEFAULT_TYPE = "TEXT";
    private static final String COLUMN_DEFAULT = "default";

    private static final String COLUMN_DECIMALS_TYPE = "INTEGER";
    private static final String COLUMN_DECIMALS = "decimal";

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
            	COLUMN_NAME + " " + COLUMN_NAME_TYPE + COMMA_SEP +
                COLUMN_MIN  + " " + COLUMN_MIN_TYPE + COMMA_SEP +
                COLUMN_MAX + " " + COLUMN_MAX_TYPE + COMMA_SEP +
                COLUMN_DEFAULT + " " + COLUMN_DEFAULT_TYPE + COMMA_SEP +
                COLUMN_DECIMALS + " " + COLUMN_DECIMALS_TYPE + COMMA_SEP +
            ")";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;	
    
    
    /**
     * Insert a min/max object into the database
     * @param context
     * @param minMax
     */
    public static void insertMinMax(Context context, FieldMinMax minMax) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues values = minMaxToContentValues(minMax);
        long postId = database.insert(FieldMinMaxORM.TABLE_NAME, "null", values);

        database.close();
    }

    /**
     * Get a min/max object from the database.
     * @param context
     * @param name
     * @return
     */
    public static FieldMinMax getMinMax(Context context, String name) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + "WHERE name='" + name + "'", null);

        FieldMinMax field = null;
        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            field = cursorToMinMax(cursor);
            // name is unique so we do not get any more
        }

        database.close();

        return field;
    }
    
    /**
     * Packs a FieldMinMax object into a ContentValues map for use with SQL inserts.
     */
    private static ContentValues minMaxToContentValues(FieldMinMax minMax) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, minMax.getName());
        values.put(COLUMN_MIN, minMax.getMin());
        values.put(COLUMN_MAX, minMax.getMax());
        values.put(COLUMN_DEFAULT, minMax.getDefaultValue());
        values.put(COLUMN_DECIMALS, minMax.getDecimals());
        return values;
    }    
    

    /**
     * Populates a FieldMinMax object with data from a Cursor
     * @param cursor
     * @return
     */
    private static FieldMinMax cursorToMinMax(Cursor cursor) {
    	String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
    	double min = cursor.getDouble(cursor.getColumnIndex(COLUMN_MIN));
    	double max = cursor.getDouble(cursor.getColumnIndex(COLUMN_MAX));
    	double defaultValue = cursor.getDouble(cursor.getColumnIndex(COLUMN_DEFAULT));
    	double decimals = cursor.getDouble(cursor.getColumnIndex(COLUMN_DECIMALS));
    	
    	FieldMinMax minMax = new FieldMinMax(name, min, max, defaultValue, decimals);

        return minMax;
    }    
	
}
