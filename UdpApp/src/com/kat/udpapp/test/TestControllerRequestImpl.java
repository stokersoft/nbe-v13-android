/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 */

package com.kat.udpapp.test;

import java.util.Map;

import com.kat.udpapp.ControllerRequest;
import com.kat.udpapp.ControllerRequestImpl;
import com.kat.udpapp.ControllerResponseImpl;
import com.kat.udpapp.exception.ParseException;

public class TestControllerRequestImpl {
/*
	@Test
	public void testRequest() {
		
		ControllerRequestImpl cr = new ControllerRequestImpl();
		
		cr.setFunctionId(0);
		cr.setCode("0123456789");
		cr.setPayload(new String("this is payload").getBytes());
		
		byte[] rawRequest = cr.getRawRequest();
		
		if (rawRequest.length!=(ControllerRequest.HEADER_SIZE + new String("this is payload").length())) {
			fail("Wrong length");
		}
		
		if (rawRequest[0]!=ControllerRequest.STX) {
			fail("Not starting with STX");
		}
		
		if (rawRequest[1]!=0x30 || rawRequest[2]!=0x30) {
			fail("Function id not correct");
		}
		
		
		if (rawRequest[3]!=0x30 || rawRequest[12]!=0x39) {
			fail("Code not correct");			
		}
		
		if (rawRequest[13]!='0' || rawRequest[14]!='1' || rawRequest[15]!='5') {
			fail("Payload size not correct");			
		}
		
		if (rawRequest[(ControllerRequest.HEADER_SIZE + new String("this is payload").length())-1]!=ControllerRequest.EOT) {
			fail("Not ending with EOT");
		}

		System.out.print(new String(rawRequest));
				
	}
	
	@Test
	public void testDiscoveryResponse()  {
		
		ControllerResponseImpl cr = new ControllerResponseImpl();
		
		// Test a OK discovery response
		
		String dataStr = "X000028Serial=10257;IP=192.168.1.38X";
		byte[] data = dataStr.getBytes();
		data[0] = ControllerRequest.STX;
		data[data.length-1] = ControllerRequest.EOT;
		
		try {
			cr.setData(data);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		
		String values[] = cr.getDiscoveryValues();
		
		String ip = values[0];
		String serial = values[1];
		
		if (!ip.equals("192.168.1.38")) {
			fail("Bad ip read");
		}
		
		if (!serial.equals("10257")) {
			fail("Bad serial read");
		}
		
		// Test an errorphrone discovery response
		dataStr = "X000028Serial=10257;IP=192.168.1.38X";
		data = dataStr.getBytes();
		data[0] = ControllerRequest.STX;
		//data[data.length-1] = ControllerRequest.EOT;
		
		try {
			cr.setData(data);
			fail("No exception thrown, expected one");
		} catch (ParseException e) {
		}

		// Test an errorphrone discovery response
		dataStr = "X000028Serial=10257;IP=192.168.1.38X";
		data = dataStr.getBytes();
		data[data.length-1] = ControllerRequest.EOT;
		
		try {
			cr.setData(data);
			fail("No exception thrown, expected one");
		} catch (ParseException e) {
		}
		
		// Test an errorphrone discovery response, length is too long
		dataStr = "X000029Serial=10257;IP=192.168.1.38X";
		data = dataStr.getBytes();
		data[0] = ControllerRequest.STX;
		data[data.length-1] = ControllerRequest.EOT;
		
		try {
			cr.setData(data);
			fail("No exception thrown, expected one");
		} catch (Exception e) {
		}
		
		
		
	}
	
	@Test
	public void testReadResponse() {		
		ControllerResponseImpl cr = new ControllerResponseImpl();

		String dataStr = "X010133temp=48;diff_over=10;diff_under=5;reduction=0;ext_stop_temp=0.0;ext_stop_diff=1.0;ext_switch=0;ext_off_delay=1;ext_on_delay=1;timer=0X";
		byte[] data = dataStr.getBytes();
		data[0] = ControllerRequest.STX;
		data[data.length-1] = ControllerRequest.EOT;
		
		try {
			cr.setData(data);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		
		Map<String, String> map = null;
		try {
			map = cr.getReadValues();
		} catch (ParseException e) {
			fail(e.getMessage());
		}
		
		String value = map.get("temp");
		if (!value.equals("48")) {
			fail("Temp not ok");
		}

		value = map.get("diff_over");
		if (!value.equals("10")) {
			fail("diff_over not ok");
		}

		value = map.get("timer");
		if (!value.equals("0")) {
			fail("diff_over not ok");
		}
				
	}*/
}
