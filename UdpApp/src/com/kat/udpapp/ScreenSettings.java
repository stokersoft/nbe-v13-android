package com.kat.udpapp;

import java.util.HashMap;

import pl.polidea.view.ZoomView;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kat.udpapp.DataListDialogFragment.DataDialogListener;
import com.kat.udpapp.helper.LanguageLoaderSingleton;

public class ScreenSettings extends FragmentActivity implements
		OnClickListener, DataDialogListener {

	LinearLayout llBack;
	Button btnSave;
	private ZoomView zoomView;
	LinearLayout main_container;

	TextView temp;
	String tempKey;

	HashMap<String, Integer> selectedValues;

	TextView txtBoilerTitle, txtWeatherTitle, txtHopperTitle, txtDHWTitle,
			txtHeaderTop;
	TextView txtBoiler1, txtBoiler2, txtBoiler3, txtBoiler4, txtBoiler5,
			txtBoiler6, txtBoiler7, txtBoiler8, txtBoiler9, txtBoiler10,
			txtBoiler11;
	TextView txtWeather1, txtWeather2, txtWeather3, txtWeather4, txtWeather5,
			txtWeather6, txtWeather7, txtWeather8, txtWeather9, txtWeather10,
			txtWeather11;
	TextView txtDHW1, txtDHW2, txtDHW3, txtDHW4, txtDHW5, txtDHW6, txtDHW7,
			txtDHW8, txtDHW9, txtDHW10, txtDHW11;
	TextView txtHopper1, txtHopper2, txtHopper3, txtHopper4, txtHopper5,
			txtHopper6, txtHopper7, txtHopper8, txtHopper9, txtHopper10,
			txtHopper11;

	ImageView imgBoiler1, imgBoiler2, imgBoiler3, imgBoiler4, imgBoiler5,
			imgBoiler6, imgBoiler7, imgBoiler8, imgBoiler9, imgBoiler10,
			imgBoiler11;
	ImageView imgWeather1, imgWeather2, imgWeather3, imgWeather4, imgWeather5,
			imgWeather6, imgWeather7, imgWeather8, imgWeather9, imgWeather10,
			imgWeather11;
	ImageView imgDHW1, imgDHW2, imgDHW3, imgDHW4, imgDHW5, imgDHW6, imgDHW7,
			imgDHW8, imgDHW9, imgDHW10, imgDHW11;
	ImageView imgHopper1, imgHopper2, imgHopper3, imgHopper4, imgHopper5,
			imgHopper6, imgHopper7, imgHopper8, imgHopper9, imgHopper10,
			imgHopper11;

	TextView txtHelp, txtBack;

	TextView txtSpinnerBoiler1, txtSpinnerBoiler2, txtSpinnerBoiler3,
			txtSpinnerBoiler4, txtSpinnerBoiler5, txtSpinnerBoiler6,
			txtSpinnerBoiler7, txtSpinnerBoiler8, txtSpinnerBoiler9,
			txtSpinnerBoiler10, txtSpinnerBoiler11;

	TextView txtSpinnerDHW1, txtSpinnerDHW2, txtSpinnerDHW3, txtSpinnerDHW4,
			txtSpinnerDHW5, txtSpinnerDHW6, txtSpinnerDHW7, txtSpinnerDHW8,
			txtSpinnerDHW9, txtSpinnerDHW10, txtSpinnerDHW11;

	TextView txtSpinnerHopper1, txtSpinnerHopper2, txtSpinnerHopper3,
			txtSpinnerHopper4, txtSpinnerHopper5, txtSpinnerHopper6,
			txtSpinnerHopper7, txtSpinnerHopper8, txtSpinnerHopper9,
			txtSpinnerHopper10, txtSpinnerHopper11;

	TextView txtSpinnerWeather1, txtSpinnerWeather2, txtSpinnerWeather3,
			txtSpinnerWeather4, txtSpinnerWeather5, txtSpinnerWeather6,
			txtSpinnerWeather7, txtSpinnerWeather8, txtSpinnerWeather9,
			txtSpinnerWeather10, txtSpinnerWeather11;

	Context context = ScreenSettings.this;
	View v;

	String[] boiler, DHW, Hopper, Weather;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_second);

		v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.screen_settings, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		boiler = Constants.boiler;
		DHW = Constants.DHW;
		Hopper = Constants.Hopper;
		Weather = Constants.weather;

		llBack = (LinearLayout) v.findViewById(R.id.llBack);
		btnSave = (Button) v.findViewById(R.id.btnSave);

		selectedValues = new HashMap<String, Integer>();

		llBack.setOnClickListener(ScreenSettings.this);
		btnSave.setOnClickListener(ScreenSettings.this);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				new settingViews().execute();
			}
		}, 200);
		// new SettingAdapter().execute();

	}

	public class settingViews extends AsyncTask<Void, Void, Void> implements
			OnClickListener {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			zoomView = new ZoomView(ScreenSettings.this);
			zoomView.addView(v);
			txtBoilerTitle = (TextView) v.findViewById(R.id.txtTItle);
			txtWeatherTitle = (TextView) v.findViewById(R.id.a);
			txtHopperTitle = (TextView) v.findViewById(R.id.txtAuto);
			txtDHWTitle = (TextView) v.findViewById(R.id.txtDHWTitle);
			txtHeaderTop = (TextView) v.findViewById(R.id.txtHeaderTop);

			txtHelp = (TextView) v.findViewById(R.id.txtHelp);
			txtBack = (TextView) v.findViewById(R.id.txtBack);

			txtSpinnerBoiler1 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler1);
			txtSpinnerBoiler2 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler2);
			txtSpinnerBoiler3 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler3);
			txtSpinnerBoiler4 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler4);
			txtSpinnerBoiler5 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler5);
			txtSpinnerBoiler6 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler6);
			txtSpinnerBoiler7 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler7);
			txtSpinnerBoiler8 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler8);
			txtSpinnerBoiler9 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler9);
			txtSpinnerBoiler10 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler10);
			txtSpinnerBoiler11 = (TextView) v
					.findViewById(R.id.txtSpinnerBoiler11);

			txtSpinnerDHW1 = (TextView) v.findViewById(R.id.txtSpinnerDHW1);
			txtSpinnerDHW2 = (TextView) v.findViewById(R.id.txtSpinnerDHW2);
			txtSpinnerDHW3 = (TextView) v.findViewById(R.id.txtSpinnerDHW3);
			txtSpinnerDHW4 = (TextView) v.findViewById(R.id.txtSpinnerDHW4);
			txtSpinnerDHW5 = (TextView) v.findViewById(R.id.txtSpinnerDHW5);
			txtSpinnerDHW6 = (TextView) v.findViewById(R.id.txtSpinnerDHW6);
			txtSpinnerDHW7 = (TextView) v.findViewById(R.id.txtSpinnerDHW7);
			txtSpinnerDHW8 = (TextView) v.findViewById(R.id.txtSpinnerDHW8);
			txtSpinnerDHW9 = (TextView) v.findViewById(R.id.txtSpinnerDHW9);
			txtSpinnerDHW10 = (TextView) v.findViewById(R.id.txtSpinnerDHW10);
			txtSpinnerDHW11 = (TextView) v.findViewById(R.id.txtSpinnerDHW11);

			txtSpinnerWeather1 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather1);
			txtSpinnerWeather2 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather2);
			txtSpinnerWeather3 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather3);
			txtSpinnerWeather4 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather4);
			txtSpinnerWeather5 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather5);
			txtSpinnerWeather6 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather6);
			txtSpinnerWeather7 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather7);
			txtSpinnerWeather8 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather8);
			txtSpinnerWeather9 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather9);
			txtSpinnerWeather10 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather10);
			txtSpinnerWeather11 = (TextView) v
					.findViewById(R.id.txtSpinnerWeather11);

			txtSpinnerHopper1 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper1);
			txtSpinnerHopper2 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper2);
			txtSpinnerHopper3 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper3);
			txtSpinnerHopper4 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper4);
			txtSpinnerHopper5 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper5);
			txtSpinnerHopper6 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper6);
			txtSpinnerHopper7 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper7);
			txtSpinnerHopper8 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper8);
			txtSpinnerHopper9 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper9);
			txtSpinnerHopper10 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper10);
			txtSpinnerHopper11 = (TextView) v
					.findViewById(R.id.txtSpinnerHopper11);

			txtWeather1 = (TextView) v.findViewById(R.id.txtWeather1);
			txtWeather2 = (TextView) v.findViewById(R.id.txtWeather2);
			txtWeather3 = (TextView) v.findViewById(R.id.txtWeather3);
			txtWeather4 = (TextView) v.findViewById(R.id.txtWeather4);
			txtWeather5 = (TextView) v.findViewById(R.id.txtWeather5);
			txtWeather6 = (TextView) v.findViewById(R.id.txtWeather6);
			txtWeather7 = (TextView) v.findViewById(R.id.txtWeather7);
			txtWeather8 = (TextView) v.findViewById(R.id.txtWeather8);
			txtWeather9 = (TextView) v.findViewById(R.id.txtWeather9);
			txtWeather10 = (TextView) v.findViewById(R.id.txtWeather10);
			txtWeather11 = (TextView) v.findViewById(R.id.txtWeather11);

			txtDHW1 = (TextView) v.findViewById(R.id.txtDHW1);
			txtDHW2 = (TextView) v.findViewById(R.id.txtDHW2);
			txtDHW3 = (TextView) v.findViewById(R.id.txtDHW3);
			txtDHW4 = (TextView) v.findViewById(R.id.txtDHW4);
			txtDHW5 = (TextView) v.findViewById(R.id.txtDHW5);
			txtDHW6 = (TextView) v.findViewById(R.id.txtDHW6);
			txtDHW7 = (TextView) v.findViewById(R.id.txtDHW7);
			txtDHW8 = (TextView) v.findViewById(R.id.txtDHW8);
			txtDHW9 = (TextView) v.findViewById(R.id.txtDHW9);
			txtDHW10 = (TextView) v.findViewById(R.id.txtDHW10);
			txtDHW11 = (TextView) v.findViewById(R.id.txtDHW11);

			txtHopper1 = (TextView) v.findViewById(R.id.txtHopper1);
			txtHopper2 = (TextView) v.findViewById(R.id.txtHopper2);
			txtHopper3 = (TextView) v.findViewById(R.id.txtHopper3);
			txtHopper4 = (TextView) v.findViewById(R.id.txtHopper4);
			txtHopper5 = (TextView) v.findViewById(R.id.txtHopper5);
			txtHopper6 = (TextView) v.findViewById(R.id.txtHopper6);
			txtHopper7 = (TextView) v.findViewById(R.id.txtHopper7);
			txtHopper8 = (TextView) v.findViewById(R.id.txtHopper8);
			txtHopper9 = (TextView) v.findViewById(R.id.txtHopper9);
			txtHopper10 = (TextView) v.findViewById(R.id.txtHopper10);
			txtHopper11 = (TextView) v.findViewById(R.id.txtHopper11);

			txtBoiler1 = (TextView) v.findViewById(R.id.txtBoiler1);
			txtBoiler2 = (TextView) v.findViewById(R.id.txtBoiler2);
			txtBoiler3 = (TextView) v.findViewById(R.id.txtBoiler3);
			txtBoiler4 = (TextView) v.findViewById(R.id.txtBoiler4);
			txtBoiler5 = (TextView) v.findViewById(R.id.txtBoiler5);
			txtBoiler6 = (TextView) v.findViewById(R.id.txtBoiler6);
			txtBoiler7 = (TextView) v.findViewById(R.id.txtBoiler7);
			txtBoiler8 = (TextView) v.findViewById(R.id.txtBoiler8);
			txtBoiler9 = (TextView) v.findViewById(R.id.txtBoiler9);
			txtBoiler10 = (TextView) v.findViewById(R.id.txtBoiler10);
			txtBoiler11 = (TextView) v.findViewById(R.id.txtBoiler11);

			SharedPreferences preferences = context.getSharedPreferences(
					Constants.language, Context.MODE_PRIVATE);

			txtBack.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_button_back"));
			txtHelp.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_button_help"));
			txtBoilerTitle.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_button_boiler"));
			txtBoiler1.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler1_head"));
			txtBoiler2.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler2_head"));
			txtBoiler3.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler3_head"));
			txtBoiler4.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler4_head"));
			txtBoiler5.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler5_head"));
			txtBoiler6.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler6_head"));
			txtBoiler7.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler7_head"));
					
			txtBoiler8.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler8_head"));
					
			txtBoiler9.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_boiler9_head"));
					
			txtBoiler10.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_boiler10_head"));
			txtBoiler11.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_boiler11_head"));

			txtHeaderTop.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_settings"));
			txtHopperTitle.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_button_hopper"));
			txtHopper1.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper1_head"));
			txtHopper2.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper2_head"));
			txtHopper3.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper3_head"));
			txtHopper4.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper4_head"));
			txtHopper5.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper5_head"));
			txtHopper6.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper6_head"));
			txtHopper7.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper7_head"));
			txtHopper8.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper8_head"));
			txtHopper9.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_hopper9_head"));
			txtHopper10.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_hopper10_head"));
			txtHopper11.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_hopper11_head"));

			txtWeatherTitle.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_button_weather"));
			txtWeather1.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather1_head"));
			txtWeather2.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather2_head"));
			txtWeather3.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather3_head"));
			txtWeather4.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather4_head"));
			txtWeather5.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather5_head"));
			txtWeather6.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather6_head"));
			txtWeather7.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather7_head"));
			txtWeather8.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather8_head"));
			txtWeather9.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather9_head"));
			txtWeather10.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather10_head"));
			txtWeather11.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_weather11_head"));
			
			txtDHWTitle.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_button_dhw"));
			txtDHW1.setText(LanguageLoaderSingleton.getStringFromLanguage(
					"lng_screen_dhw1_head"));
			
			
			
			
			txtDHW2.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw2_head"));
			txtDHW3.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw3_head"));
			txtDHW4.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw4_head"));
			txtDHW5.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw5_head"));
			txtDHW6.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw6_head"));
			txtDHW7.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw7_head"));
			txtDHW8.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw8_head"));
			txtDHW9.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw9_head"));
			txtDHW10.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw10_head"));
			txtDHW11.setText(LanguageLoaderSingleton.getStringFromLanguage("lng_screen_dhw11_head"));

			// ImageView of Help icon
			imgBoiler1 = (ImageView) v.findViewById(R.id.imgBoiler1);
			imgBoiler2 = (ImageView) v.findViewById(R.id.imgBoiler2);
			imgBoiler3 = (ImageView) v.findViewById(R.id.imgBoiler3);
			imgBoiler4 = (ImageView) v.findViewById(R.id.imgBoiler4);
			imgBoiler5 = (ImageView) v.findViewById(R.id.imgBoiler5);
			imgBoiler6 = (ImageView) v.findViewById(R.id.imgBoiler6);
			imgBoiler7 = (ImageView) v.findViewById(R.id.imgBoiler7);
			imgBoiler8 = (ImageView) v.findViewById(R.id.imgBoiler8);
			imgBoiler9 = (ImageView) v.findViewById(R.id.imgBoiler9);
			imgBoiler10 = (ImageView) v.findViewById(R.id.imgBoiler10);
			imgBoiler11 = (ImageView) v.findViewById(R.id.imgBoiler11);

			imgDHW1 = (ImageView) v.findViewById(R.id.imgDHW1);
			imgDHW2 = (ImageView) v.findViewById(R.id.imgDHW2);
			imgDHW3 = (ImageView) v.findViewById(R.id.imgDHW3);
			imgDHW4 = (ImageView) v.findViewById(R.id.imgDHW4);
			imgDHW5 = (ImageView) v.findViewById(R.id.imgDHW5);
			imgDHW6 = (ImageView) v.findViewById(R.id.imgDHW6);
			imgDHW7 = (ImageView) v.findViewById(R.id.imgDHW7);
			imgDHW8 = (ImageView) v.findViewById(R.id.imgDHW8);
			imgDHW9 = (ImageView) v.findViewById(R.id.imgDHW9);
			imgDHW10 = (ImageView) v.findViewById(R.id.imgDHW10);
			imgDHW11 = (ImageView) v.findViewById(R.id.imgDHW11);

			imgHopper1 = (ImageView) v.findViewById(R.id.imgHopper1);
			imgHopper2 = (ImageView) v.findViewById(R.id.imgHopper2);
			imgHopper3 = (ImageView) v.findViewById(R.id.imgHopper3);
			imgHopper4 = (ImageView) v.findViewById(R.id.imgHopper4);
			imgHopper5 = (ImageView) v.findViewById(R.id.imgHopper5);
			imgHopper6 = (ImageView) v.findViewById(R.id.imgHopper6);
			imgHopper7 = (ImageView) v.findViewById(R.id.imgHopper7);
			imgHopper8 = (ImageView) v.findViewById(R.id.imgHopper8);
			imgHopper9 = (ImageView) v.findViewById(R.id.imgHopper9);
			imgHopper10 = (ImageView) v.findViewById(R.id.imgHopper10);
			imgHopper11 = (ImageView) v.findViewById(R.id.imgHopper11);

			imgWeather1 = (ImageView) v.findViewById(R.id.imgWeather1);
			imgWeather2 = (ImageView) v.findViewById(R.id.imgWeather2);
			imgWeather3 = (ImageView) v.findViewById(R.id.imgWeather3);
			imgWeather4 = (ImageView) v.findViewById(R.id.imgWeather4);
			imgWeather5 = (ImageView) v.findViewById(R.id.imgWeather5);
			imgWeather6 = (ImageView) v.findViewById(R.id.imgWeather6);
			imgWeather7 = (ImageView) v.findViewById(R.id.imgWeather7);
			imgWeather8 = (ImageView) v.findViewById(R.id.imgWeather8);
			imgWeather9 = (ImageView) v.findViewById(R.id.imgWeather9);
			imgWeather10 = (ImageView) v.findViewById(R.id.imgWeather10);
			imgWeather11 = (ImageView) v.findViewById(R.id.imgWeather11);

			imgBoiler1.setOnClickListener(this);
			imgBoiler2.setOnClickListener(this);
			imgBoiler3.setOnClickListener(this);
			imgBoiler4.setOnClickListener(this);
			imgBoiler5.setOnClickListener(this);
			imgBoiler6.setOnClickListener(this);
			imgBoiler7.setOnClickListener(this);
			imgBoiler8.setOnClickListener(this);
			imgBoiler9.setOnClickListener(this);
			imgBoiler10.setOnClickListener(this);
			imgBoiler11.setOnClickListener(this);

			imgHopper1.setOnClickListener(this);
			imgHopper2.setOnClickListener(this);
			imgHopper3.setOnClickListener(this);
			imgHopper4.setOnClickListener(this);
			imgHopper5.setOnClickListener(this);
			imgHopper6.setOnClickListener(this);
			imgHopper7.setOnClickListener(this);
			imgHopper8.setOnClickListener(this);
			imgHopper9.setOnClickListener(this);
			imgHopper10.setOnClickListener(this);
			imgHopper11.setOnClickListener(this);

			imgDHW1.setOnClickListener(this);
			imgDHW2.setOnClickListener(this);
			imgDHW3.setOnClickListener(this);
			imgDHW4.setOnClickListener(this);
			imgDHW5.setOnClickListener(this);
			imgDHW6.setOnClickListener(this);
			imgDHW7.setOnClickListener(this);
			imgDHW8.setOnClickListener(this);
			imgDHW9.setOnClickListener(this);
			imgDHW10.setOnClickListener(this);
			imgDHW11.setOnClickListener(this);

			imgWeather1.setOnClickListener(this);
			imgWeather2.setOnClickListener(this);
			imgWeather3.setOnClickListener(this);
			imgWeather4.setOnClickListener(this);
			imgWeather5.setOnClickListener(this);
			imgWeather6.setOnClickListener(this);
			imgWeather7.setOnClickListener(this);
			imgWeather8.setOnClickListener(this);
			imgWeather9.setOnClickListener(this);
			imgWeather10.setOnClickListener(this);
			imgWeather11.setOnClickListener(this);

			preferences = context.getSharedPreferences("appData",
					Context.MODE_PRIVATE);

			txtSpinnerBoiler1.setText(boiler[preferences.getInt("kedel1", 0)]);
			txtSpinnerBoiler2.setText(boiler[preferences.getInt("kedel2", 0)]);
			txtSpinnerBoiler3.setText(boiler[preferences.getInt("kedel3", 0)]);
			txtSpinnerBoiler4.setText(boiler[preferences.getInt("kedel4", 0)]);
			txtSpinnerBoiler5.setText(boiler[preferences.getInt("kedel5", 0)]);
			txtSpinnerBoiler6.setText(boiler[preferences.getInt("kedel6", 0)]);
			txtSpinnerBoiler7.setText(boiler[preferences.getInt("kedel7", 0)]);
			txtSpinnerBoiler8.setText(boiler[preferences.getInt("kedel8", 0)]);
			txtSpinnerBoiler9.setText(boiler[preferences.getInt("kedel9", 0)]);
			txtSpinnerBoiler10
					.setText(boiler[preferences.getInt("kedel10", 0)]);
			txtSpinnerBoiler11
					.setText(boiler[preferences.getInt("kedel11", 0)]);

			txtSpinnerDHW1.setText(DHW[preferences.getInt("vvb1", 0)]);
			txtSpinnerDHW2.setText(DHW[preferences.getInt("vvb2", 0)]);
			txtSpinnerDHW3.setText(DHW[preferences.getInt("vvb3", 0)]);
			txtSpinnerDHW4.setText(DHW[preferences.getInt("vvb4", 0)]);
			txtSpinnerDHW5.setText(DHW[preferences.getInt("vvb5", 0)]);
			txtSpinnerDHW6.setText(DHW[preferences.getInt("vvb6", 0)]);
			txtSpinnerDHW7.setText(DHW[preferences.getInt("vvb7", 0)]);
			txtSpinnerDHW8.setText(DHW[preferences.getInt("vvb8", 0)]);
			txtSpinnerDHW9.setText(DHW[preferences.getInt("vvb9", 0)]);
			txtSpinnerDHW10.setText(DHW[preferences.getInt("vvb10", 0)]);
			txtSpinnerDHW11.setText(DHW[preferences.getInt("vvb11", 0)]);

			txtSpinnerHopper1.setText(Hopper[preferences.getInt("silo1", 0)]);
			txtSpinnerHopper2.setText(Hopper[preferences.getInt("silo2", 0)]);
			txtSpinnerHopper3.setText(Hopper[preferences.getInt("silo3", 0)]);
			txtSpinnerHopper4.setText(Hopper[preferences.getInt("silo4", 0)]);
			txtSpinnerHopper5.setText(Hopper[preferences.getInt("silo5", 0)]);
			txtSpinnerHopper6.setText(Hopper[preferences.getInt("silo6", 0)]);
			txtSpinnerHopper7.setText(Hopper[preferences.getInt("silo7", 0)]);
			txtSpinnerHopper8.setText(Hopper[preferences.getInt("silo8", 0)]);
			txtSpinnerHopper9.setText(Hopper[preferences.getInt("silo9", 0)]);
			txtSpinnerHopper10.setText(Hopper[preferences.getInt("silo10", 0)]);
			txtSpinnerHopper11.setText(Hopper[preferences.getInt("silo11", 0)]);

			txtSpinnerWeather1.setText(Weather[preferences.getInt("vejr1", 0)]);
			txtSpinnerWeather2.setText(Weather[preferences.getInt("vejr2", 0)]);
			txtSpinnerWeather3.setText(Weather[preferences.getInt("vejr3", 0)]);
			txtSpinnerWeather4.setText(Weather[preferences.getInt("vejr4", 0)]);
			txtSpinnerWeather5.setText(Weather[preferences.getInt("vejr5", 0)]);
			txtSpinnerWeather6.setText(Weather[preferences.getInt("vejr6", 0)]);
			txtSpinnerWeather7.setText(Weather[preferences.getInt("vejr7", 0)]);
			txtSpinnerWeather8.setText(Weather[preferences.getInt("vejr8", 0)]);
			txtSpinnerWeather9.setText(Weather[preferences.getInt("vejr9", 0)]);
			txtSpinnerWeather10
					.setText(Weather[preferences.getInt("vejr10", 0)]);
			txtSpinnerWeather11
					.setText(Weather[preferences.getInt("vejr11", 0)]);

			txtSpinnerBoiler1.setOnClickListener(this);
			txtSpinnerBoiler2.setOnClickListener(this);
			txtSpinnerBoiler3.setOnClickListener(this);
			txtSpinnerBoiler4.setOnClickListener(this);
			txtSpinnerBoiler5.setOnClickListener(this);
			txtSpinnerBoiler6.setOnClickListener(this);
			txtSpinnerBoiler7.setOnClickListener(this);
			txtSpinnerBoiler8.setOnClickListener(this);
			txtSpinnerBoiler9.setOnClickListener(this);
			txtSpinnerBoiler10.setOnClickListener(this);
			txtSpinnerBoiler11.setOnClickListener(this);

			txtSpinnerDHW1.setOnClickListener(this);
			txtSpinnerDHW2.setOnClickListener(this);
			txtSpinnerDHW3.setOnClickListener(this);
			txtSpinnerDHW4.setOnClickListener(this);
			txtSpinnerDHW5.setOnClickListener(this);
			txtSpinnerDHW6.setOnClickListener(this);
			txtSpinnerDHW7.setOnClickListener(this);
			txtSpinnerDHW8.setOnClickListener(this);
			txtSpinnerDHW9.setOnClickListener(this);
			txtSpinnerDHW10.setOnClickListener(this);
			txtSpinnerDHW11.setOnClickListener(this);

			txtSpinnerHopper1.setOnClickListener(this);
			txtSpinnerHopper2.setOnClickListener(this);
			txtSpinnerHopper3.setOnClickListener(this);
			txtSpinnerHopper4.setOnClickListener(this);
			txtSpinnerHopper5.setOnClickListener(this);
			txtSpinnerHopper6.setOnClickListener(this);
			txtSpinnerHopper7.setOnClickListener(this);
			txtSpinnerHopper8.setOnClickListener(this);
			txtSpinnerHopper9.setOnClickListener(this);
			txtSpinnerHopper10.setOnClickListener(this);
			txtSpinnerHopper11.setOnClickListener(this);

			txtSpinnerWeather1.setOnClickListener(this);
			txtSpinnerWeather2.setOnClickListener(this);
			txtSpinnerWeather3.setOnClickListener(this);
			txtSpinnerWeather4.setOnClickListener(this);
			txtSpinnerWeather5.setOnClickListener(this);
			txtSpinnerWeather6.setOnClickListener(this);
			txtSpinnerWeather7.setOnClickListener(this);
			txtSpinnerWeather8.setOnClickListener(this);
			txtSpinnerWeather9.setOnClickListener(this);
			txtSpinnerWeather10.setOnClickListener(this);
			txtSpinnerWeather11.setOnClickListener(this);
			

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				main_container = (LinearLayout) findViewById(R.id.main_container);
				main_container.addView(zoomView);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onClick(View v) {
			if (v == txtSpinnerBoiler1) {
				temp = txtSpinnerBoiler1;
				tempKey = "kedel1";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler2) {
				temp = txtSpinnerBoiler2;
				tempKey = "kedel2";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler3) {
				temp = txtSpinnerBoiler3;
				tempKey = "kedel3";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler4) {
				temp = txtSpinnerBoiler4;
				tempKey = "kedel4";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler5) {
				temp = txtSpinnerBoiler5;
				tempKey = "kedel5";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler6) {
				temp = txtSpinnerBoiler6;
				tempKey = "kedel6";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler7) {
				temp = txtSpinnerBoiler7;
				tempKey = "kedel7";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler8) {
				temp = txtSpinnerBoiler8;
				tempKey = "kedel8";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler9) {
				temp = txtSpinnerBoiler9;
				tempKey = "kedel9";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler10) {
				temp = txtSpinnerBoiler10;
				tempKey = "kedel10";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerBoiler11) {
				temp = txtSpinnerBoiler11;
				tempKey = "kedel11";
				showDataListDialog(boiler);
			} else if (v == txtSpinnerDHW1) {
				temp = txtSpinnerDHW1;
				tempKey = "vvb1";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW2) {
				temp = txtSpinnerDHW2;
				tempKey = "vvb2";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW3) {
				temp = txtSpinnerDHW3;
				tempKey = "vvb3";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW4) {
				temp = txtSpinnerDHW4;
				tempKey = "vvb4";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW5) {
				temp = txtSpinnerDHW5;
				tempKey = "vvb5";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW6) {
				temp = txtSpinnerDHW6;
				tempKey = "vvb6";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW7) {
				temp = txtSpinnerDHW7;
				tempKey = "vvb7";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW8) {
				temp = txtSpinnerDHW8;
				tempKey = "vvb8";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW9) {
				temp = txtSpinnerDHW9;
				tempKey = "vvb9";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW10) {
				temp = txtSpinnerDHW10;
				tempKey = "vvb10";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerDHW11) {
				temp = txtSpinnerDHW11;
				tempKey = "vvb11";
				showDataListDialog(DHW);
			} else if (v == txtSpinnerHopper1) {
				temp = txtSpinnerHopper1;
				tempKey = "silo1";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper2) {
				temp = txtSpinnerHopper2;
				tempKey = "silo2";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper3) {
				temp = txtSpinnerHopper3;
				tempKey = "silo3";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper4) {
				temp = txtSpinnerHopper4;
				tempKey = "silo4";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper5) {
				temp = txtSpinnerHopper5;
				tempKey = "silo5";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper6) {
				temp = txtSpinnerHopper6;
				tempKey = "silo6";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper7) {
				temp = txtSpinnerHopper7;
				tempKey = "silo7";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper8) {
				temp = txtSpinnerHopper8;
				tempKey = "silo8";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper9) {
				temp = txtSpinnerHopper9;
				tempKey = "silo9";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper10) {
				temp = txtSpinnerHopper10;
				tempKey = "silo10";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerHopper11) {
				temp = txtSpinnerHopper11;
				tempKey = "silo11";
				showDataListDialog(Hopper);
			} else if (v == txtSpinnerWeather1) {
				temp = txtSpinnerWeather1;
				tempKey = "vejr1";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather2) {
				temp = txtSpinnerWeather2;
				tempKey = "vejr2";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather3) {
				temp = txtSpinnerWeather3;
				tempKey = "vejr3";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather4) {
				temp = txtSpinnerWeather4;
				tempKey = "vejr4";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather5) {
				temp = txtSpinnerWeather5;
				tempKey = "vejr5";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather6) {
				temp = txtSpinnerWeather6;
				tempKey = "vejr6";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather7) {
				temp = txtSpinnerWeather7;
				tempKey = "vejr7";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather8) {
				temp = txtSpinnerWeather8;
				tempKey = "vejr8";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather9) {
				temp = txtSpinnerWeather9;
				tempKey = "vejr9";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather10) {
				temp = txtSpinnerWeather10;
				tempKey = "vejr10";
				showDataListDialog(Weather);
			} else if (v == txtSpinnerWeather11) {
				temp = txtSpinnerWeather11;
				tempKey = "vejr11";
				showDataListDialog(Weather);
			}

			if (v == imgBoiler1 || v == imgBoiler2 || v == imgBoiler3
					|| v == imgBoiler4 || v == imgBoiler5 || v == imgBoiler6
					|| v == imgBoiler7 || v == imgBoiler8 || v == imgBoiler9
					|| v == imgBoiler10 || v == imgBoiler11) {
				showTip(SecurePreferences.getStringPreference(
						Constants.language, ScreenSettings.this,
						"lng_screen_boiler1_head_tp"));
			} else if (v == imgHopper1 || v == imgHopper2 || v == imgHopper3
					|| v == imgHopper4 || v == imgHopper5 || v == imgHopper6
					|| v == imgHopper7 || v == imgHopper8 || v == imgHopper9
					|| v == imgHopper10 || v == imgHopper11) {
				showTip(SecurePreferences.getStringPreference(
						Constants.language, ScreenSettings.this,
						"lng_screen_hopper1_head_tp"));
			} else if (v == imgDHW1 || v == imgDHW2 || v == imgDHW3
					|| v == imgDHW4 || v == imgDHW5 || v == imgDHW6
					|| v == imgDHW7 || v == imgDHW8 || v == imgDHW9
					|| v == imgDHW10 || v == imgDHW11) {
				showTip(SecurePreferences.getStringPreference(
						Constants.language, ScreenSettings.this,
						"lng_screen_dhw1_head_tp"));
			} else if (v == imgWeather1 || v == imgWeather2 || v == imgWeather3
					|| v == imgWeather4 || v == imgWeather5 || v == imgWeather6
					|| v == imgWeather7 || v == imgWeather8 || v == imgWeather9
					|| v == imgWeather10 || v == imgWeather11) {
				showTip(SecurePreferences.getStringPreference(
						Constants.language, ScreenSettings.this,
						"lng_screen_weather1_head_tp"));
			}

		}
	}

	private void showDataListDialog(String[] array) {
		FragmentManager fm = getSupportFragmentManager();
		DataListDialogFragment editNameDialog = new DataListDialogFragment();
		Bundle args = new Bundle();
		args.putStringArray("data", array);
		editNameDialog.setArguments(args);
		editNameDialog.show(fm, "fragment_country_list");
	}

	@Override
	public void onDataSelected(int result) {
		if (tempKey.contains("kedel")) {
			temp.setText(boiler[result]);

		} else if (tempKey.contains("vvb")) {
			temp.setText(DHW[result]);

		} else if (tempKey.contains("silo")) {
			temp.setText(Hopper[result]);

		} else if (tempKey.contains("vejr")) {
			temp.setText(Weather[result]);
		}

		selectedValues.put(tempKey, result);
		temp = null;
		tempKey = "";
	}

	protected void showTip(String tip) {
		final Dialog dialog = new Dialog(ScreenSettings.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dailog_tip);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		TextView txtTip = (TextView) dialog.findViewById(R.id.txtTIP);
		txtTip.setText(tip);
		Button btnOK = (Button) dialog.findViewById(R.id.btnOK);

		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	@Override
	public void onClick(View v) {
		if (v == llBack) {
			onBackPressed();
		} else if (v == btnSave) {
			Context context = this;
			for (int i = 0; i < 11; i++) {
				if (selectedValues.get("kedel" + (i + 1)) != null) {
					SecurePreferences.savePreferences(context, "kedel"
							+ (i + 1), selectedValues.get("kedel" + (i + 1)));
				}
			}

			for (int i = 0; i < 11; i++) {
				if (selectedValues.get("vvb" + (i + 1)) != null) {
					SecurePreferences.savePreferences(context, "vvb" + (i + 1),
							selectedValues.get("vvb" + (i + 1)));
				}
			}

			for (int i = 0; i < 11; i++) {
				if (selectedValues.get("silo" + (i + 1)) != null) {
					SecurePreferences.savePreferences(context,
							"silo" + (i + 1),
							selectedValues.get("silo" + (i + 1)));
				}
			}
			
			
			for (int i = 0; i < 11; i++) {
				if (selectedValues.get("vejr" + (i + 1)) != null) {
					SecurePreferences.savePreferences(context,
							"vejr" + (i + 1),
							selectedValues.get("vejr" + (i + 1)));
				}
			}
			
			
			
			
			
			
			
			
			
			
			
			


			Toast.makeText(ScreenSettings.this, "Saved", Toast.LENGTH_SHORT)
					.show();
		}
	}

}
