package com.kat.udpapp;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.Toast;

import com.kat.adaptors.EventsAdaptor;
import com.kat.models.Events;
import com.kat.udpapp.exception.ParseException;
import com.kat.xmlprofile.helpers.ApplicationGlobles;
import com.kat.xmlprofile.helpers.Logs;

public class EventsViewPage extends Activity implements OnScrollListener {
	public static EventsAdaptor mAdapter;
	ListView listView;
	public String DataToLoad;
	Handler m_handler;
	Runnable m_handlerTask;
	public OldEventsTask oldEventsTask = new OldEventsTask();
	public com.gc.materialdesign.views.ProgressBarCircularIndeterminate progressBar;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_events_view_page);
		progressBar=(com.gc.materialdesign.views.ProgressBarCircularIndeterminate)findViewById(R.id.progressBar);
		progressBar.setVisibility(View.INVISIBLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		Events.SetEventImagesFromImagesID();
		ApplicationGlobles.loadStrictModePolicies();
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
		String formattedDate = df.format(c.getTime());
		String[] Splitted = formattedDate.split(" ");
		String[] date = Splitted[0].split("-");
		String[] time = Splitted[1].split(":");
		String sendDate = date[0] + date[1] + date[2];
		String sendTime = time[0] + (time[1]) + time[2];
		// DataToLoad = "150717-134300";
		DataToLoad = sendDate + "-" + sendTime;
		mAdapter = new EventsAdaptor(EventsViewPage.this);
		listView = (ListView) findViewById(R.id.listView1);
		listView.setAdapter(mAdapter);
		listView.setOnScrollListener(this);
		NewEventsTask newEventsTask = new NewEventsTask();
		newEventsTask.execute();

		

		m_handler = new Handler();
		m_handlerTask = new Runnable() {
			@Override
			public void run() {

				//

				Calendar c = Calendar.getInstance();

				SimpleDateFormat df = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
				String formattedDate = df.format(c.getTime());
				String[] Splitted = formattedDate.split(" ");

				String[] date = Splitted[0].split("-");
				String[] time = Splitted[1].split(":");

				String sendDate = date[0] + date[1] + date[2];
				String sendTime = time[0] + (time[1]) + time[2];

				// DataToLoad = "150717-134300";

				DataToLoad = sendDate + "-" + sendTime;

				NewEventsTask newEventsTask = new NewEventsTask();
				newEventsTask.execute();

				m_handler.postDelayed(m_handlerTask, 8000);

			}
		};
		m_handlerTask.run();

	}

	public class NewEventsTask extends
			AsyncTask<ArrayList<Events>, Void, ArrayList<Events>> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected ArrayList<Events> doInBackground(ArrayList<Events>... params) {
			ArrayList<Events> arrayList = new ArrayList<Events>(0);
			try {

				arrayList = Events.getEvents(DataToLoad);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<Events> ArrayeventsNew) {
			super.onPostExecute(ArrayeventsNew);

			if (mAdapter.getAllData().size() == 0) {

				for (int i = 0; i < ArrayeventsNew.size(); i++) {

					if (ArrayeventsNew.get(i).getIsHeader() == true)

					{
						Logs.createLog("Added Header");

						mAdapter.addSectionHeaderItem(ArrayeventsNew.get(i));

					} else {
						Logs.createLog("Added New Data");
						mAdapter.addItem(i, ArrayeventsNew.get(i));

					}

				}
			} else {

				ArrayeventsNew.trimToSize();

				for (int i = 0; i < ArrayeventsNew.size(); i++) {

					{
						if (isDuplicate(mAdapter.getAllData(),
								ArrayeventsNew.get(i)))

						{

						} else {

							Logs.createLog("Added New Data");
							mAdapter.getAllData().get(0).setIsHeader(false);
							ArrayeventsNew.get(1).setIsHeader(true);

							mAdapter.addItem(0, ArrayeventsNew.get(i));
						}

					}

				}

			}

			mAdapter.notifyDataSetChanged();
			listView.invalidate();

		}

	}

	public static boolean isDuplicate(List<Events> list, Events event) {
		boolean result = false;
		for (Events e : list) {

			if (e.getEventText().toString()
					.equals(event.getEventText().toString())
					&& e.getTime().toString()
							.equals(event.getTime().toString())
					&& e.getDate().toString()
							.equals(event.getDate().toString())
					&& e.getFirstValue().toString()
							.equals(event.getFirstValue().toString())) {
				result = true;
				break;

			} else {

			}
		}
		return result;

	}

	public class OldEventsTask extends
			AsyncTask<ArrayList<Events>, Void, ArrayList<Events>> {

		
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected ArrayList<Events> doInBackground(ArrayList<Events>... params) {
			ArrayList<Events> arrayList = new ArrayList<Events>(0);
			try {

				arrayList = Events.getEvents(DataToLoad);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return arrayList;
		}

		@Override
		protected void onPostExecute(ArrayList<Events> ArrayeventsNew) {
			super.onPostExecute(ArrayeventsNew);

			ArrayeventsNew.trimToSize();

			for (int i = 0; i < ArrayeventsNew.size(); i++) {

				{
					if (isDuplicate(mAdapter.getAllData(),
							ArrayeventsNew.get(i)))

					{

					} else {

						Logs.createLog("Added New Data");

						int length = mAdapter.getAllData().size();
						mAdapter.addItem(length - 1 + 1, ArrayeventsNew.get(i));
						mAdapter.notifyDataSetChanged();
					}

				}

			}

			mAdapter.notifyDataSetChanged();
			listView.invalidate();

		}

	}

	// Code To Remove Duplicacy

	public void back(View v) {

		finish();
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

		int position = firstVisibleItem + visibleItemCount;
		int limit = totalItemCount;

		// Check if bottom has been reached
		if (position >= limit && totalItemCount > 1) {
			// scroll end reached
			// Write your code here
			
			if(mAdapter.getAllData().size()>0)
			{
		
			Events eventLast = mAdapter.getItem(mAdapter.getAllData()
					.size() - 1);

			DataToLoad = eventLast.Date + "-" + eventLast.Time;
			oldEventsTask = new OldEventsTask();
			oldEventsTask.execute();
			}
		}
	}

}
