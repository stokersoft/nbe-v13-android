/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 */
package com.kat.udpapp;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kat.models.Events;
import com.kat.udpapp.exception.ParseException;

public interface ControllerResponse {

	public static final int STX = 2;
	public static final int EOT = 4;
	public static final int FUNCTION_ID_MAX = 5;
	public static final int STATUS_CODE_MAX = 3;
	public static final int PAYLOAD_MAX = 1017;
	
	public byte[] getPayload();
	public int getFunctionId();
	public int getStatusCode();
	public InetAddress getSenderAddress();
	public void setSenderAddress(InetAddress sender);
	public long getReceiveTime();
	public void setReceiveTime(long time);
	public byte[] getRawData();
	
	public void setData(byte[] response) throws ParseException;
	public String[] getDiscoveryValues();
	public Map<String, String> getReadValues() throws ParseException;
	public Map<String, String> getOperationValues() throws ParseException;
	public Map<String, String> getConsuptionValues() throws ParseException;
	public Set<Events> getEventValues() throws ParseException;
	public List<Integer> getInfoValues() throws ParseException;
	public String getSingleReadValue() throws ParseException;
	public Map<String, String> getMinMaxValues() throws ParseException;
		
}
