package com.kat.udpapp;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SecurePreferences {

	public static void savePreferences(String preferenceName, Context context,
			String key, String value) {
		SharedPreferences preferences = context.getSharedPreferences(
				preferenceName, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void savePreferences(Context context, String key, String value) {
		SharedPreferences preferences = context.getSharedPreferences("appData",
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		try {

			byte ptext[] = value.getBytes("UTF-8");

			String valueOf = new String(ptext, "UTF-8");
			editor.putString(key, valueOf);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void savePreferences(Context context, String key, int value) {
		SharedPreferences preferences = context.getSharedPreferences("appData",
				Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		try {
			editor.putInt(key, value);
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getIntPreference(Context context, String key) {
		SharedPreferences preferences = context.getSharedPreferences("appData",
				Context.MODE_PRIVATE);
		int value = preferences.getInt(key, 0);
		return value;
	}

	public static String getStringPreference(String preferenceName,
			Context context, String key) {
		SharedPreferences preferences = context.getSharedPreferences(
				preferenceName, Context.MODE_PRIVATE);
		String value = preferences.getString(key, "");
		return value;
	}

	public static String getStringPreference(Context context, String key) {
		SharedPreferences preferences = context.getSharedPreferences("appData",
				Context.MODE_PRIVATE);
		String value = preferences.getString(key, "");
		return value;
	}

	public static void savePreferences(String preferenceName, Context context,
			ArrayList<String> key) {
		SharedPreferences preferences = context.getSharedPreferences(
				preferenceName, Context.MODE_PRIVATE);

		Editor editor = null;
		editor = preferences.edit();
		for (int i = 0; i < key.size(); i++) {
			try {
				String line = key.get(i);
				int middle = line.indexOf("=");
				
				byte ptext[] = line.substring(middle + 1, line.length())
						.getBytes("UTF-8");
				String valueOf = new String(ptext, "UTF-8");
				editor.putString(line.substring(0, middle), valueOf);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (editor != null) {
			editor.commit();
		}

	}
}
