/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 */
package com.kat.udpapp;

public interface ControllerRequest {

	public static final int STX = 2;
	public static final int EOT = 4;
	public static final int CODE_SIZE = 10;
	public static final int HEADER_SIZE = 17;

	public static final int FUNCTION_DISCOVER = 0;
	public static final int FUNCTION_READ = 1;
	public static final int FUNCTION_SET = 2;
	public static final int FUNCTION_READ_MIN_MAX_DEFAULT = 3;
	public static final int FUNCTION_READ_OPERATION_DATA = 4;
	public static final int FUNCTION_READ_CHART_DATA = 5;
	public static final int FUNCTION_READ_EVENT_LOG = 8;
	public static final int FUNCTION_INFO_ITEMS = 9;
	
	public static final int FUNCTION_ID_6 = 6;
	
	
	public void setFunctionId(int functionId);
	public void setCode(String code);
	public void setPayload(byte[] payload);
	
	/**
	 * Create a request, by handcrafting the bytes, nothing is changed, when passed to send function.
	 * 
	 * @param data The raw data bytes that should be sent including STX and EOT
	 */
	public void setRawData(byte[] data);
	
	
	public byte[] getRawRequest();
	
	public void setReadRequest(String password, String value);
	
	public void setSetRequest(String password, String name, String value);
	
	public void setOperationRequest(String password, String value);
	
	public void setConsuptionRequest(String password, String value);
	
	public void setEventRequest(String password, String value);

	public void setInfoRequest(String password, String value);
	
	public void setMinMaxRequest(String password, String value);

	
}
