package com.kat.udpapp;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Map.Entry;

import com.buganalytics.trace.BugAnalytics;
import com.kat.udpapp.helper.LanguageLoaderSingleton;
import com.kat.xmlprofile.helpers.Logs;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class ConsuptionActivity extends FragmentActivity implements
		OnClickListener {

	TextView txtHeaderTop, txtBack, txtHelp;

	private String password, myIP;

	private InetAddress controllerAddress;
	private InetAddress myAddress;
	private NBEControllerClient client;

	ViewPager viewPager;
	PagerTabStrip strip;

	View v;

	LinearLayout main_container;

	Toast toast;

	LinearLayout llBack;

	RadioGroup rgp;
	SparseArray<RadioButton> buttonID;
	WakeLock wakeLock;
	static String[] totalHourArray = { null }, dhwHourArray = { null };

	static String[] totalDaysArray = { null }, dhwDaysArray = { null };
	static String[] totalMonthsArray = { null }, dhwMonthsArray = { null };
	static String[] totalYearsArray = { null }, dhwYearsArray = { null };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_second);

		v = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE))
				.inflate(R.layout.consumption_activity_swipe, null, false);
		v.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		Logs.createLog("Custum View Set OnCreate AS ",
				"consumption_activity_swipe");

		viewPager = (ViewPager) v.findViewById(R.id.pager);
		strip = (PagerTabStrip) v.findViewById(R.id.strip);
		rgp = (RadioGroup) v.findViewById(R.id.radioGroup1);

		buttonID = new SparseArray<RadioButton>();
		buttonID.put(0, (RadioButton) rgp.getChildAt(0));
		buttonID.put(1, (RadioButton) rgp.getChildAt(1));
		buttonID.put(2, (RadioButton) rgp.getChildAt(2));
		buttonID.put(3, (RadioButton) rgp.getChildAt(3));

		for (int i = 0; i < strip.getChildCount(); ++i) {
			View nextChild = strip.getChildAt(i);
			if (nextChild instanceof TextView) {
				TextView textViewToConvert = (TextView) nextChild;
				Typeface tf = Typeface.createFromAsset(this.getAssets(),
						"OPENSANS-LIGHT.TTF");
				textViewToConvert.setTypeface(tf);
				textViewToConvert.setTextSize(getResources().getDimension(
						R.dimen.onezero));
			}
		}

		viewPager.setBackgroundColor(this.getResources().getColor(
				android.R.color.transparent));

		password = Constants.password;
		try {
			controllerAddress = InetAddress.getByName(Constants.controller);
			myIP = getLocalIpAddress();
			myAddress = InetAddress.getByName(myIP);
			client = NBEControllerClient.getInstance();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		txtHelp = (TextView) v.findViewById(R.id.txtHelp);
		txtBack = (TextView) v.findViewById(R.id.txtBack);
		txtHeaderTop = (TextView) v.findViewById(R.id.txtHeaderTop);
		llBack = (LinearLayout) v.findViewById(R.id.llBack);

		new getConsuptionData().execute();

		llBack.setOnClickListener(ConsuptionActivity.this);

		txtBack.setText(LanguageLoaderSingleton
				.getStringFromLanguage("lng_button_back"));
		txtHelp.setText(LanguageLoaderSingleton
				.getStringFromLanguage("lng_button_help"));

		txtHeaderTop.setText(LanguageLoaderSingleton
				.getStringFromLanguage("lng_button_consumption"));

		try {
			main_container = (LinearLayout) findViewById(R.id.main_container);
			main_container.addView(v);
		} catch (Exception e) {
			e.printStackTrace();
		}

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				rgp.check((buttonID.get(position)).getId());
			}

			@Override
			public void onPageScrolled(int position, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
		Logs.createLog("Setting Wakelock For Screen Dim Issue");

		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK,
				"Full Wake Lock");

	}

	public String getLocalIpAddress() {
		try {
			String ip = null;
			ConnectivityManager connManager = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			WifiManager wifiMan = (WifiManager) this
					.getSystemService(Context.WIFI_SERVICE);
			if (wifiMan.isWifiEnabled()) {
				WifiInfo wifiInf = wifiMan.getConnectionInfo();

				int ipAddress = wifiInf.getIpAddress();
				ip = String.format("%d.%d.%d.%d", (ipAddress & 0xff),
						(ipAddress >> 8 & 0xff), (ipAddress >> 16 & 0xff),
						(ipAddress >> 24 & 0xff));
				return ip;
			} else if ((netInfo != null) && netInfo.isConnected()) {
				for (Enumeration<NetworkInterface> en = NetworkInterface
						.getNetworkInterfaces(); en.hasMoreElements();) {
					NetworkInterface intf = en.nextElement();
					for (Enumeration<InetAddress> enumIpAddr = intf
							.getInetAddresses(); enumIpAddr.hasMoreElements();) {
						InetAddress inetAddress = enumIpAddr.nextElement();
						if (!inetAddress.isLoopbackAddress()) {
							ip = inetAddress.getHostAddress().toString();
						}
					}
				}
				return ip;
			}
		} catch (Exception ex) {
			Log.e("IP Address", ex.toString());
		}
		return null;
	}

	public class getConsuptionData extends AsyncTask<Void, Void, Void> {

		ControllerResponse response;
		ControllerRequest cr;
		Map<String, String> map;
		boolean error = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			toast = Toast.makeText(ConsuptionActivity.this, "Loading Charts",
					Toast.LENGTH_SHORT);
			toast.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			try {
				error = false;
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				Logs.createLog("Get Consuption Data:", response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {

					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));

					totalHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {

					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					totalDaysArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					totalMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "total_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					totalYearsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_hours");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					dhwHourArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_days");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					dhwDaysArray = entry.getValue().split(",");

				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_months");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					dhwMonthsArray = entry.getValue().split(",");
				}
				cr = new ControllerRequestImpl();
				cr.setConsuptionRequest(password, "dhw_years");
				response = client.sendRequest(myAddress, controllerAddress, cr);
				System.out.println(response.toString());
				map = response.getConsuptionValues();
				for (Entry<String, String> entry : map.entrySet()) {
					Logs.createLog(String.valueOf(entry.getKey()) + ": ",
							String.valueOf(entry.getValue()));
					dhwYearsArray = entry.getValue().split(",");
				}
			} catch (Exception e) {
				e.printStackTrace();
				error = true;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (error) {
				new getConsuptionData().execute();
			} else {
				if (toast != null) {
					toast.cancel();
				}
				toast = Toast.makeText(ConsuptionActivity.this, "Complete",
						Toast.LENGTH_SHORT);
				toast.show();
				viewPager.setAdapter(new ConsuptionViewPagerAdapter(
						getSupportFragmentManager()));

				rgp.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						int radioButtonID = group.getCheckedRadioButtonId();
						View radioButton = group.findViewById(radioButtonID);
						int idx = group.indexOfChild(radioButton);
						viewPager.setCurrentItem(idx);
					}
				});
			}
			super.onPostExecute(result);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == llBack) {
			onBackPressed();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		wakeLock.acquire();
		Logs.createLog("Wakelock: ", "  wakeLock.acquire");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		wakeLock.release();
		Logs.createLog("Wakelock: ", "  wakeLock.release");
	}

	@Override
	protected void onStart() {
		Logs.createLog("BugAnalytics ", "Started");
		// BugAnalytics Api Starts Here
		BugAnalytics.startSession(this);
		super.onStart();
	}

	@Override
	protected void onStop() {
		// BugAnalytics Api Stops Here
		BugAnalytics.closeSession(this);
		Logs.createLog("BugAnalytics ", "Stopped");
		super.onStop();
	}
}