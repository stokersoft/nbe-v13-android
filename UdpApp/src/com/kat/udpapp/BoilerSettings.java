package com.kat.udpapp;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kat.adaptors.SpinnerArrayAdaptor;
import com.kat.models.BoilerSettingMenu;
import com.kat.models.BoilerSettingSubMenuFields;
import com.kat.models.DropDownfieldsets;
import com.kat.models.DropDownfieldsetsFields;
import com.kat.udpapp.exception.ParseException;
import com.kat.udpapp.utils.RangeSeekBar;
import com.kat.udpapp.utils.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.kat.xmlprofile.helpers.ApplicationGlobles;
import com.kat.xmlprofile.helpers.Logs;

public class BoilerSettings extends Activity {
	public static final String LOG_TAG = "BoilerSettings.java";
	public static BoilerSettingMenu boilerSettingMenu = new BoilerSettingMenu();
	LinearLayout LinearLayoutMainContainer, lvSubmenuFields, layoutSetTwoCome;

	private NBEControllerClient client;
	private InetAddress controllerAddress, controllerAddressTemp;
	private InetAddress myAddress;
	String myIP = null;
	ArrayList<BoilerSettingSubMenuFields> newboilerFieldSetsFieldValues = new ArrayList<BoilerSettingSubMenuFields>();
	ArrayList<BoilerSettingSubMenuFields> oldboilerFieldSetsFieldValues = new ArrayList<BoilerSettingSubMenuFields>();
	ArrayList<BoilerSettingSubMenuFields> finalBoilerSettingSubMenuFields = new ArrayList<BoilerSettingSubMenuFields>();
	public static final int TEXT_SIZE_XHDPI = 18;
	public static final int TEXT_SIZE_HDPI = 13;
	public static final int TEXT_SIZE_MDPI = 9;
	public static final int TEXT_SIZE_LDPI = 8;
	public RangeSeekBar<Double> rangeBar;
	public TextView TextViewMin;
	public TextView TextViewMax;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boiler_settings);

		ActionBar actionBar = getActionBar();
		actionBar.hide();
		Logs.createLog("onCreate ");
		ApplicationGlobles.hideKeyboard(BoilerSettings.this);
		Logs.createLog("hideKeyboard ");
		findViewsById();
		Logs.createLog("findViewsById");
		AsyncCaller asyncCaller = new AsyncCaller();
		asyncCaller.execute();
		Logs.createLog("Async Task to Load Boiler Settings ");
	}

	public void Refresh(View view) {
		Logs.createLog("Reloading boiler Settings");
		LinearLayoutMainContainer.removeAllViews();
		AsyncCaller asyncCaller = new AsyncCaller();
		asyncCaller.execute();

	}

	private class AsyncCaller extends AsyncTask<Void, Void, Void> {
		ProgressDialog pdLoading = new ProgressDialog(BoilerSettings.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("Loading Settings...");
			pdLoading.setCancelable(false);
			pdLoading.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			loadBoilerSettingsFromXMlDom();

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			// this method will be running on UI thread
			CreateCustomViewsFromBoilerSettings();
			pdLoading.dismiss();
		}

	}

	private void findViewsById() {
		LinearLayoutMainContainer = (LinearLayout) findViewById(R.id.LinearLayoutMainContainer);

	}

	private void loadBoilerSettingsFromXMlDom() {
		boilerSettingMenu = BoilerSettingMenu
				.getboilerSettingsFromXml(getApplicationContext());

	}

	private void CreateCustomViewsFromBoilerSettings() {
		ControllerResponse controllerResponse = null;

		for (int i = 0; i < boilerSettingMenu.getArrayBoilerSettingSubMenu()
				.size(); i++) {

			// code checks if its new layout to create or existing then add to
			// existing

			// this helps in creating dynamic horizontal/vertical layout

			if (i % 2 == 0) {
				LayoutInflater layoutInflater = (LayoutInflater) this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view = layoutInflater.inflate(
						R.layout.boilersettingsorietation, null);

				LinearLayoutMainContainer.addView(view);

				layoutSetTwoCome = (LinearLayout) view
						.findViewById(R.id.layoutSetTwoCome);

				layoutSetTwoCome.setOrientation(LinearLayout.HORIZONTAL);

				LayoutInflater layoutInflaterData = (LayoutInflater) this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view5 = layoutInflaterData.inflate(
						R.layout.bookingmainview, null);

				RelativeLayout rl = (RelativeLayout) view5
						.findViewById(R.id.rlcont);

				rl.getLayoutParams().width = (Integer.valueOf(String
						.valueOf(getScreenSize()[0] / 2))) - 20;

				rl.invalidate();

				layoutSetTwoCome.addView(view5);

				TextView txtTItle = (TextView) view5
						.findViewById(R.id.txtTItle);

				txtTItle.setText(boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i).getTxtId());

				oldboilerFieldSetsFieldValues.addAll(boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i)
						.getBoilerSettingSubMenuFields());
				oldboilerFieldSetsFieldValues.trimToSize();

				for (int j = 0; j < boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i)
						.getBoilerSettingSubMenuFields().size(); j++)

				{

					lvSubmenuFields = (LinearLayout) view
							.findViewById(R.id.lvSubmenuFields);
					LayoutInflater inflater1 = getLayoutInflater();
					View view1 = new View(getApplicationContext());
					view1 = inflater1
							.inflate(R.layout.boilerfieldslayout, null);

					TextView textViewFieldname = (TextView) view1
							.findViewById(R.id.textViewFieldname);

					textViewFieldname.setText(boilerSettingMenu
							.getArrayBoilerSettingSubMenu().get(i)
							.getBoilerSettingSubMenuFields().get(j)
							.getFieldsTxtId());

					switch (getResources().getDisplayMetrics().densityDpi) {
					case DisplayMetrics.DENSITY_XHIGH:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_XHDPI);

						break;
					case DisplayMetrics.DENSITY_HIGH:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_HDPI);

						break;

					case DisplayMetrics.DENSITY_TV:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_HDPI);

						break;
					default:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_XHDPI);

						break;
					}

					ImageView imgHelp = (ImageView) view1
							.findViewById(R.id.imgHelp);

					imgHelp.setTag(
							R.string.BoilerSettingsSetViewTagUniqueIDOne,
							boilerSettingMenu.getArrayBoilerSettingSubMenu()
									.get(i).getBoilerSettingSubMenuFields()
									.get(j).getFieldsTxtId());
					imgHelp.setTag(
							R.string.BoilerSettingsSetViewTagUniqueIDTwo,
							boilerSettingMenu.getArrayBoilerSettingSubMenu()
									.get(i).getBoilerSettingSubMenuFields()
									.get(j).getFieldshelptxtid());

					imgHelp.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							showTip(String
									.valueOf(v
											.getTag(R.string.BoilerSettingsSetViewTagUniqueIDOne)),
									String.valueOf(v
											.getTag(R.string.BoilerSettingsSetViewTagUniqueIDTwo)));

						}
					});

					LinearLayout lvCustomControl = (LinearLayout) view1
							.findViewById(R.id.lvCustomControl);
					lvCustomControl.setTag("lvCustomControl");

					// Code to check Either New Dynamic Input Control is Edit
					// Text Or Spinner * Most Important

					if (boilerSettingMenu.getArrayBoilerSettingSubMenu().get(i)
							.getBoilerSettingSubMenuFields().get(j).Fieldstype
							.equals("text")) {

						final EditText edt = new EditText(
								getApplicationContext());
						edt.setFocusable(false);
						edt.setInputType(InputType.TYPE_CLASS_NUMBER);
						edt.setImeOptions(EditorInfo.IME_ACTION_DONE);
						edt.setTag(boilerSettingMenu
								.getArrayBoilerSettingSubMenu().get(i)
								.getBoilerSettingSubMenuFields().get(j)
								.getFieldudp());
						edt.setBackgroundColor(Color.WHITE);
						edt.setTextColor(Color.BLACK);

						edt.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								AlertDialog.Builder alert = new AlertDialog.Builder(
										BoilerSettings.this);

								alert.setTitle("Set Boiler Settings");

								LayoutInflater inflater = getLayoutInflater();
								FrameLayout f1 = new FrameLayout(
										getApplicationContext());
								f1.addView(inflater.inflate(
										R.layout.boilersliderpopup, f1, false));
								alert.setView(f1);

								alert.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

												dialog.cancel();

											}
										});
								LinearLayout getroute = (LinearLayout) f1
										.findViewById(R.id.layoutSeekbar);

								rangeBar = (RangeSeekBar<Double>) getroute
										.findViewById(R.id.seekBar2);
								rangeBar.setNotifyWhileDragging(true);
								rangeBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Double>() {

									@Override
									public void onRangeSeekBarValuesChanged(
											RangeSeekBar<?> bar,
											Double minValue, Double maxValue) {
										// TODO Auto-generated method stub

										edt.setText(String.valueOf(maxValue));

										edt.invalidate();

									}
								});

								TextViewMin = (TextView) f1
										.findViewById(R.id.TextViewMin);
								TextViewMax = (TextView) f1
										.findViewById(R.id.TextViewMax);
								// Set the range

								final Map<String, String> values = new HashMap<String, String>();

								values.put("FieldName",
										String.valueOf(edt.getTag()));

								String[] splitedText = String.valueOf(
										edt.getTag()).split("\\.");

								List<FieldMinMaxSugarOrm> MinMax = FieldMinMaxSugarOrm
										.getSugarOrmMinMax(
												getApplicationContext(),
												splitedText[1]);
								if (MinMax.size() == 0)

								{

									rangeBar.setTag(edt.getTag());
									GetMinMaxTask getMinMaxTask = new GetMinMaxTask();

									getMinMaxTask.execute(values);
								}

								else

								{

									rangeBar.setRangeValues(
											MinMax.get(0).min,
											MinMax.get(0).max);

									TextViewMin.setText("Minimum Value: "
											+ String.valueOf(MinMax.get(0).min))
													;

									TextViewMax.setText("Maximum Value: "
											+ String.valueOf(MinMax.get(0).max));

								}

								//

								// Showing Alert Message
								alert.show();

							}
						});

						// Code To Check It We Have Done Controller Call So That
						// Second Time It Gets Data From That

						if (j == 0) {
							// its Call For First Time
							String Fieldudp = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[0]
									+ "";

							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";

							controllerResponse = getData(Fieldudp + ".*");

							Map<String, String> map;
							try {

								try {
									map = controllerResponse.getReadValues();
									Object value = map.get(FieldRest);
									edt.setText(String.valueOf(value));
									
									
									
									rangeBar.setSelectedMaxValue(Double.valueOf(String.valueOf(value)));
							
									rangeBar.invalidate();
									edt.invalidate();

									boilerSettingMenu
											.getArrayBoilerSettingSubMenu()
											.get(i)
											.getBoilerSettingSubMenuFields()
											.get(j)
											.setFieldValue(
													String.valueOf(value));

								} catch (NullPointerException e) {

								}

							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {

							// its Call For Second Time
							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";
							Map<String, String> map;
							try {
								map = controllerResponse.getReadValues();
								Object value = map.get(FieldRest);
								edt.setText(String.valueOf(value));

								edt.invalidate();
								boilerSettingMenu
										.getArrayBoilerSettingSubMenu().get(i)
										.getBoilerSettingSubMenuFields().get(j)
										.setFieldValue(String.valueOf(value));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						lvCustomControl.addView(edt);
					} else

					{

						// if Dynamic Input Control is Spinner in FirstLayout *
						// Most Important

						Spinner spinner = new Spinner(getApplicationContext());
						spinner.setTag(boilerSettingMenu
								.getArrayBoilerSettingSubMenu().get(i)
								.getBoilerSettingSubMenuFields().get(j)
								.getFieldudp());

						ArrayList<DropDownfieldsets> downfieldsetsFields = boilerSettingMenu
								.getArrayDropDownfieldsets();

						String itemgetDropDownfieldsetsName;

						for (DropDownfieldsets item : downfieldsetsFields) {

							if (item.getDropDownfieldsetsName() == boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldset())

							{
								DropDownfieldsets items = item;

								SpinnerArrayAdaptor dataAdapter = new SpinnerArrayAdaptor(
										BoilerSettings.this,
										R.layout.layout_spinner,
										item.getDropDownfieldsetsFields());
								spinner.setBackgroundColor(Color
										.parseColor("#ffffff"));

								spinner.setAdapter(dataAdapter);

								// Code To Check It We Have Done Controller Call
								// So That Second Time It Gets Data From That
								if (j == 0) {
									String Fieldudp = boilerSettingMenu
											.getArrayBoilerSettingSubMenu()
											.get(i)
											.getBoilerSettingSubMenuFields()
											.get(j).getFieldudp().split("\\.")[0]
											+ "";

									String FieldRest = boilerSettingMenu
											.getArrayBoilerSettingSubMenu()
											.get(i)
											.getBoilerSettingSubMenuFields()
											.get(j).getFieldudp().split("\\.")[1]
											+ "";

									controllerResponse = getData(Fieldudp
											+ ".*");

									Map<String, String> map;
									try {

										try {
											map = controllerResponse
													.getReadValues();
											Object value = map.get(FieldRest);
											// spinner.setSelection(Integer
											// .valueOf(String.valueOf(value)));

											boilerSettingMenu
													.getArrayBoilerSettingSubMenu()
													.get(i)
													.getBoilerSettingSubMenuFields()
													.get(j)
													.setFieldValue(
															String.valueOf(value));
										} catch (NullPointerException e) {
											System.out
													.print("Caught the NullPointerException");
										}

									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								} else {

									// Code To get data from controllerResponse
									// to set in spinner

									String FieldRest = boilerSettingMenu
											.getArrayBoilerSettingSubMenu()
											.get(i)
											.getBoilerSettingSubMenuFields()
											.get(j).getFieldudp().split("\\.")[1]
											+ "";
									Map<String, String> map;
									try {
										map = controllerResponse
												.getReadValues();
										Object value = map.get(FieldRest);
										// spinner.setSelection(Integer
										// .valueOf(String.valueOf(value)));
										boilerSettingMenu
												.getArrayBoilerSettingSubMenu()
												.get(i)
												.getBoilerSettingSubMenuFields()
												.get(j)
												.setFieldValue(
														String.valueOf(value));
									} catch (ParseException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

								}

								break;

							}

						}

						lvCustomControl.addView(spinner);
					}

					lvSubmenuFields.addView(view1, j);

				}

			} else {

				// Code To Create Right Side Dynamic Layout

				LayoutInflater layoutInflaterData = (LayoutInflater) this
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view5 = layoutInflaterData.inflate(
						R.layout.bookingmainview, null);

				RelativeLayout rl = (RelativeLayout) view5
						.findViewById(R.id.rlcont);

				rl.getLayoutParams().width = (Integer.valueOf(String
						.valueOf(getScreenSize()[0] / 2))) - 20;

				rl.invalidate();

				layoutSetTwoCome.addView(view5);

				TextView txtTItle = (TextView) view5
						.findViewById(R.id.txtTItle);

				txtTItle.setText(boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i).getTxtId());

				oldboilerFieldSetsFieldValues.addAll(boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i)
						.getBoilerSettingSubMenuFields());

				for (int j = 0; j < boilerSettingMenu
						.getArrayBoilerSettingSubMenu().get(i)
						.getBoilerSettingSubMenuFields().size(); j++)

				{

					lvSubmenuFields = (LinearLayout) view5
							.findViewById(R.id.lvSubmenuFields);
					LayoutInflater inflater1 = getLayoutInflater();
					View view1 = new View(getApplicationContext());
					view1 = inflater1
							.inflate(R.layout.boilerfieldslayout, null);

					TextView textViewFieldname = (TextView) view1
							.findViewById(R.id.textViewFieldname);

					textViewFieldname.setText(boilerSettingMenu
							.getArrayBoilerSettingSubMenu().get(i)
							.getBoilerSettingSubMenuFields().get(j)
							.getFieldsTxtId());

					switch (getResources().getDisplayMetrics().densityDpi) {
					case DisplayMetrics.DENSITY_XHIGH:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_XHDPI);

						break;
					case DisplayMetrics.DENSITY_HIGH:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_HDPI);

						break;

					case DisplayMetrics.DENSITY_TV:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_HDPI);

						break;
					default:
						textViewFieldname
								.setTextSize(BoilerSettings.TEXT_SIZE_XHDPI);

						break;
					}

					ImageView imgHelp = (ImageView) view1
							.findViewById(R.id.imgHelp);

					imgHelp.setTag(
							R.string.BoilerSettingsSetViewTagUniqueIDOne,
							boilerSettingMenu.getArrayBoilerSettingSubMenu()
									.get(i).getBoilerSettingSubMenuFields()
									.get(j).getFieldsTxtId());
					imgHelp.setTag(
							R.string.BoilerSettingsSetViewTagUniqueIDTwo,
							boilerSettingMenu.getArrayBoilerSettingSubMenu()
									.get(i).getBoilerSettingSubMenuFields()
									.get(j).getFieldshelptxtid());

					imgHelp.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							showTip(String
									.valueOf(v
											.getTag(R.string.BoilerSettingsSetViewTagUniqueIDOne)),
									String.valueOf(v
											.getTag(R.string.BoilerSettingsSetViewTagUniqueIDTwo)));

						}
					});

					LinearLayout lvCustomControl = (LinearLayout) view1
							.findViewById(R.id.lvCustomControl);

					// Code to check Either New Dynamic Input Control is Edit
					// Text Or Spinner * Most Important

					if (boilerSettingMenu.getArrayBoilerSettingSubMenu().get(i)
							.getBoilerSettingSubMenuFields().get(j).Fieldstype
							.equals("text")) {
						final EditText edt = new EditText(
								getApplicationContext());
						edt.setImeOptions(EditorInfo.IME_ACTION_DONE);
						edt.setInputType(InputType.TYPE_CLASS_NUMBER);
						edt.setBackgroundColor(Color.WHITE);
						edt.setTag(boilerSettingMenu
								.getArrayBoilerSettingSubMenu().get(i)
								.getBoilerSettingSubMenuFields().get(j)
								.getFieldudp());

						edt.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								AlertDialog.Builder alert = new AlertDialog.Builder(
										BoilerSettings.this);

								alert.setTitle("Set Boiler Settings");

								LayoutInflater inflater = getLayoutInflater();
								FrameLayout f1 = new FrameLayout(
										getApplicationContext());
								f1.addView(inflater.inflate(
										R.layout.boilersliderpopup, f1, false));
								alert.setView(f1);

								alert.setNegativeButton("Cancel",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {

												dialog.cancel();

											}
										});
								LinearLayout getroute = (LinearLayout) f1
										.findViewById(R.id.layoutSeekbar);

								rangeBar = (RangeSeekBar<Double>) getroute
										.findViewById(R.id.seekBar2);
								rangeBar.setNotifyWhileDragging(true);
								
								
								rangeBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Double>() {

									@Override
									public void onRangeSeekBarValuesChanged(
											RangeSeekBar<?> bar,
											Double minValue, Double maxValue) {
										// TODO Auto-generated method stub

										edt.setText(String.valueOf(maxValue));

										edt.invalidate();

									}
								});
								// Set the range
								TextViewMin = (TextView) f1
										.findViewById(R.id.TextViewMin);
								TextViewMax = (TextView) f1
										.findViewById(R.id.TextViewMax);
								final Map<String, String> values = new HashMap<String, String>();

								values.put("FieldName",
										String.valueOf(edt.getTag()));

								String[] splitedText = String.valueOf(
										edt.getTag()).split("\\.");

								List<FieldMinMaxSugarOrm> MinMax = FieldMinMaxSugarOrm
										.getSugarOrmMinMax(
												getApplicationContext(),
												splitedText[1]);
								if (MinMax.size() == 0)

								{

									rangeBar.setTag(edt.getTag());
									GetMinMaxTask getMinMaxTask = new GetMinMaxTask();

									getMinMaxTask.execute(values);
								}

								else

								{

									rangeBar.setRangeValues(
											MinMax.get(0).min,
											MinMax.get(0).max);
									TextViewMin.setText("Minimum Value: "
											+ String.valueOf(MinMax.get(0).min))
													;

									TextViewMax.setText("Maximum Value: "
											+ String.valueOf(MinMax.get(0).max));

								}

							
								alert.show();

							}
						});

						// Code To Check It We Have Done Controller Call So That
						// Second Time It Gets Data From That

						if (j == 0) {
							String Fieldudp = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[0]
									+ "";

							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";

							controllerResponse = getData(Fieldudp + ".*");

							Map<String, String> map;
							try {
								map = controllerResponse.getReadValues();
								Object value = map.get(FieldRest);
								edt.setText(String.valueOf(value));
								rangeBar.setSelectedMaxValue(Double.valueOf(String.valueOf( value)));
								rangeBar.refreshDrawableState();
								edt.invalidate();
								boilerSettingMenu
										.getArrayBoilerSettingSubMenu().get(i)
										.getBoilerSettingSubMenuFields().get(j)
										.setFieldValue(String.valueOf(value));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {

							// Code To get data from controllerResponse to set
							// in Edittext

							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";
							Map<String, String> map;
							try {
								map = controllerResponse.getReadValues();
								Object value = map.get(FieldRest);
								edt.setText(String.valueOf(value));
								
								rangeBar.setSelectedMaxValue(Double.valueOf(String.valueOf(value)));
						
								rangeBar.invalidate();
								edt.invalidate();
								edt.invalidate();
								boilerSettingMenu
										.getArrayBoilerSettingSubMenu().get(i)
										.getBoilerSettingSubMenuFields().get(j)
										.setFieldValue(String.valueOf(value));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						lvCustomControl.addView(edt);
					} else

					{

						// Code To get data from controllerResponse to set in
						// Spinner

						Spinner spinner = new Spinner(getApplicationContext());

						spinner.setTag(boilerSettingMenu
								.getArrayBoilerSettingSubMenu().get(i)
								.getBoilerSettingSubMenuFields().get(j)
								.getFieldudp());

						spinner.setBackgroundColor(Color.WHITE);

						ArrayList<DropDownfieldsets> downfieldsetsFields = boilerSettingMenu
								.getArrayDropDownfieldsets();

						String itemgetDropDownfieldsetsName;

						for (DropDownfieldsets item : downfieldsetsFields) {

							if (item.getDropDownfieldsetsName() == boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldset())

							{
								DropDownfieldsets items = item;

								SpinnerArrayAdaptor dataAdapter = new SpinnerArrayAdaptor(
										BoilerSettings.this,
										R.layout.layout_spinner,
										item.getDropDownfieldsetsFields());

								spinner.setAdapter(dataAdapter);

								break;

							}

						}
						// Code To Check It We Have Done Controller Call So That
						// Second Time It Gets Data From That
						if (j == 0) {
							String Fieldudp = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[0]
									+ "";

							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";

							controllerResponse = getData(Fieldudp + ".*");

							Map<String, String> map;
							try {

								try {
									map = controllerResponse.getReadValues();
									Object value = map.get(FieldRest);
									// spinner.setSelection(Integer.valueOf(String
									// .valueOf(value)));

									boilerSettingMenu
											.getArrayBoilerSettingSubMenu()
											.get(i)
											.getBoilerSettingSubMenuFields()
											.get(j)
											.setFieldValue(
													String.valueOf(value));
								} catch (NullPointerException e) {

								}

							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						} else {

							// Code To get data from controllerResponse to set
							// in spinner

							String FieldRest = boilerSettingMenu
									.getArrayBoilerSettingSubMenu().get(i)
									.getBoilerSettingSubMenuFields().get(j)
									.getFieldudp().split("\\.")[1]
									+ "";
							Map<String, String> map;
							try {
								map = controllerResponse.getReadValues();
								Object value = map.get(FieldRest);
								// spinner.setSelection(Integer.valueOf(String
								// .valueOf(value)));
								boilerSettingMenu
										.getArrayBoilerSettingSubMenu().get(i)
										.getBoilerSettingSubMenuFields().get(j)
										.setFieldValue(String.valueOf(value));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

						lvCustomControl.addView(spinner);
					}

					lvSubmenuFields.addView(view1, j);

				}

			}

			LayoutInflater inflater = getLayoutInflater();
			View view = new View(getApplicationContext());
			view = inflater.inflate(R.layout.bookingmainview, null);
			TextView txtTItle = (TextView) view.findViewById(R.id.txtTItle);

			txtTItle.setText(boilerSettingMenu.getArrayBoilerSettingSubMenu()
					.get(i).getTxtId());

		}

		int isALast = boilerSettingMenu.getArrayBoilerSettingSubMenu().size();

		if (isALast % 2 == 0) {
		} else {

			LayoutInflater layoutInflaterData = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view5 = layoutInflaterData.inflate(R.layout.bookingmainview,
					null);

			RelativeLayout rl = (RelativeLayout) view5
					.findViewById(R.id.rlcont);
			rl.getLayoutParams().width = (Integer.valueOf(String
					.valueOf(getScreenSize()[0] / 2))) - 20;

			view5.setVisibility(View.INVISIBLE);
			layoutSetTwoCome.addView(view5);

		}

	}

	public void back(View v) {

		finish();
	}

	public ControllerResponse getData(String ControllerRequiredParams) {
		ControllerResponse controllerRespons = null;
		try {
			NBEControllerClient client = NBEControllerClient.getInstance();
			String password = Constants.password;
			myIP = Constants.IpAddress;
			myAddress = InetAddress.getByName(myIP);
			controllerAddress = InetAddress.getByName(Constants.controller
					.toString().replace("/", ""));
			ControllerRequest cr = new ControllerRequestImpl();
			cr.setReadRequest(password, ControllerRequiredParams);
			ControllerResponse controllerResponse = client.sendRequest(
					myAddress, controllerAddress, cr);

			controllerRespons = controllerResponse;

			int statusCode = controllerResponse.getStatusCode();
			Logs.createLog("Controller statusCode ", String.valueOf(statusCode));
			Logs.createLog("Controller Result ",
					String.valueOf(controllerRespons));
		} catch (Exception e) {

			e.printStackTrace();
		}
		return controllerRespons;

	}

	public void setSettings(View v) throws IOException, ParseException {

		finalBoilerSettingSubMenuFields.clear();
		newboilerFieldSetsFieldValues.clear();
		ViewGroup viewGroup = (ViewGroup) findViewById(R.id.LinearLayoutMainContainer);

		findAllEdittexts(viewGroup);
		newboilerFieldSetsFieldValues.trimToSize();

		Log.d("newboilerFieldSetsFieldValues: ",
				String.valueOf(newboilerFieldSetsFieldValues.size()));

		oldboilerFieldSetsFieldValues.trimToSize();

		Log.d("oldboilerFieldSetsFieldValues: ",
				String.valueOf(oldboilerFieldSetsFieldValues.size()));

		for (BoilerSettingSubMenuFields person : newboilerFieldSetsFieldValues) {
			for (BoilerSettingSubMenuFields personNpi : oldboilerFieldSetsFieldValues) {
				if (person.getFieldudp().equals(personNpi.getFieldudp()))

				{

					if (person.getFieldValue()
							.equals(personNpi.getFieldValue())) {
						Log.e("getFieldValue", "same");
					} else {

						Log.e("Done", "Done");
						finalBoilerSettingSubMenuFields.add(person);

						new SaveSettingsTask().execute();

					}
					break;

				}

				else {
					Log.e("getFieldudp", "notsame");
				}
			}
		}

	}

	private void findAllEdittexts(ViewGroup viewGroup) {

		int count = viewGroup.getChildCount();
		for (int i = 0; i < count; i++) {
			View view = viewGroup.getChildAt(i);
			if (view instanceof ViewGroup)
				findAllEdittexts((ViewGroup) view);
			else if (view instanceof EditText) {

				EditText edittext = (EditText) view;

				BoilerSettingSubMenuFields boilerFieldSetsFieldValues = new BoilerSettingSubMenuFields();
				boilerFieldSetsFieldValues.setFieldValue(edittext.getText()
						.toString());
				boilerFieldSetsFieldValues.setFieldudp(String.valueOf(edittext
						.getTag()));
				newboilerFieldSetsFieldValues
						.add(i, boilerFieldSetsFieldValues);

			}
			if (view instanceof Spinner) {

				Spinner spinner = (Spinner) view;

				BoilerSettingSubMenuFields boilerFieldSetsFieldValues = new BoilerSettingSubMenuFields();

				DropDownfieldsetsFields downfieldsets = (DropDownfieldsetsFields) spinner
						.getSelectedItem();
				boilerFieldSetsFieldValues.setFieldValue(downfieldsets
						.getDropDownfieldsetsFieldsvalue());
				boilerFieldSetsFieldValues.setFieldudp(String.valueOf(spinner
						.getTag()));
				newboilerFieldSetsFieldValues
						.add(i, boilerFieldSetsFieldValues);
			}

		}

	}

	private int setValuesToBoiler(String UDPName, String value)
			throws IOException, ParseException {

		Logs.createLog("Set Values To  ",
				String.valueOf(UDPName + " : " + value));

		client = NBEControllerClient.getInstance();
		ControllerRequest cr = new ControllerRequestImpl();

		myAddress = InetAddress.getByName(myIP);
		controllerAddress = InetAddress.getByName(Constants.controller);
		cr.setSetRequest(Constants.password, UDPName, "" + value);
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);

		return res2.getStatusCode();

	}

	private class SaveSettingsTask extends AsyncTask<Integer, Void, Integer> {
		ProgressDialog pdLoading = new ProgressDialog(BoilerSettings.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("Saving Settings...");
			pdLoading.show();
		}

		@Override
		protected Integer doInBackground(Integer... arrayListData) {

			int reesponse = 0;

			for (int i = 0; i < finalBoilerSettingSubMenuFields.size(); i++) {
				BoilerSettingSubMenuFields boilerSettingSubMenuFields = finalBoilerSettingSubMenuFields
						.get(i);

				try {
					reesponse = setValuesToBoiler(
							boilerSettingSubMenuFields.getFieldudp(),
							boilerSettingSubMenuFields.getFieldValue());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			return reesponse;
		}

		@Override
		protected void onPostExecute(Integer reesponse) {
			super.onPostExecute(reesponse);
			if (reesponse == 0) {
				Toast.makeText(getApplicationContext(), "Saved",
						Toast.LENGTH_SHORT).show();
				pdLoading.dismiss();

			} else

			{
				Toast.makeText(getApplicationContext(),
						"Error Saving Data Please Try Later !",
						Toast.LENGTH_SHORT).show();
				pdLoading.dismiss();
			}

		}

	}

	protected void showTip(String tipTitle, String tipDetails) {

		Logs.createLog("Show Tip With Text :  ", String.valueOf(tipTitle));
		final Dialog dialog = new Dialog(BoilerSettings.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dailog_tip);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		TextView txtTip = (TextView) dialog.findViewById(R.id.txtTIP);
		txtTip.setText(tipDetails);

		TextView txtHelpTitle = (TextView) dialog
				.findViewById(R.id.txtHelpTitle);
		txtHelpTitle.setText(tipTitle);
		Button btnOK = (Button) dialog.findViewById(R.id.btnOK);

		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	public int[] getScreenSize() {
		Point size = new Point();
		WindowManager w = getWindowManager();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			w.getDefaultDisplay().getSize(size);
			return new int[] { size.x, size.y };
		} else {
			Display d = w.getDefaultDisplay();
			// noinspection deprecation
			return new int[] { d.getWidth(), d.getHeight() };
		}
	}

	private Map<String, String> getMinMaxValues(String value)
			throws IOException, ParseException {

		client = NBEControllerClient.getInstance();
		ControllerRequest cr = new ControllerRequestImpl();

		myAddress = InetAddress.getByName(myIP);
		controllerAddress = InetAddress.getByName(Constants.controller);
		cr.setMinMaxRequest(Constants.password, value);
		ControllerResponse res2 = client.sendRequest(myAddress,
				controllerAddress, cr);

		return res2.getMinMaxValues();

	}

	class GetMinMaxTask extends
			AsyncTask<Map<String, String>, Integer, Map<String, String>> {

		ProgressBar pbar = new ProgressBar(BoilerSettings.this);
		Dialog progress = new Dialog(BoilerSettings.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Log.e("onPreExecute", "Started");
			pbar.setBackgroundColor(getResources().getColor(
					android.R.color.transparent));

			progress.requestWindowFeature(Window.FEATURE_NO_TITLE);
			progress.setContentView(pbar);
			progress.getWindow().setBackgroundDrawable(
					new ColorDrawable(Color.TRANSPARENT));
			progress.show();
			Log.e("onPreExecute", "Completed");
			rangeBar.setVisibility(View.INVISIBLE);

		}

		@Override
		protected Map<String, String> doInBackground(
				Map<String, String>... params) {
			Log.e("doInBackground", "Started");

			Map<String, String> BoilerMinMax = new HashMap<String, String>();
			try {
				BoilerMinMax = getMinMaxValues(params[0].get("FieldName"));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return BoilerMinMax;
		}

		@Override
		protected void onPostExecute(Map<String, String> val) {

			progress.dismiss();

			String[] splitedText = String.valueOf(rangeBar.getTag()).split(
					"\\.");

			String splittedData[] = val.get(splitedText[1]).split(",");

			String min = String.valueOf(splittedData[0]).split("\\.")[0];

			String max = String.valueOf(splittedData[1]).split("\\.")[0];

			rangeBar.setRangeValues(Double.valueOf(min),Double.valueOf( max));
			rangeBar.setVisibility(View.VISIBLE);

			FieldMinMaxSugarOrm fieldMinMaxSugarOrm = new FieldMinMaxSugarOrm(
					splitedText[1], Double.valueOf(min), Double.valueOf(max),
					0, 0);
			FieldMinMaxSugarOrm.insertSugarOrmMinMax(getApplicationContext(),
					fieldMinMaxSugarOrm);

			TextViewMin.setText("Minimum Value: " + String.valueOf(min));

			TextViewMax.setText("Maximum Value: " + max);

			super.onPostExecute(val);

		}
	}

}