package com.kat.udpapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class StartAppBootReciever extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent _intent) {
		if (Intent.ACTION_BOOT_COMPLETED.equals(_intent.getAction())) {

			if (SecurePreferences.getIntPreference(context, "AutoStartApp") == 0) {
				Intent i = new Intent();
				i.setClassName("com.kat.udpapp", "com.kat.udpapp.MainActivity");
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
			} else {

			
			}
		}
	}
}
