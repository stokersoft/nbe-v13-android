package com.kat.udpapp;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.achartengine.util.MathHelper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.Highlight;
import com.kat.udpapp.chart.MyValueFormatter;
import com.kat.udpapp.helper.LanguageLoaderSingleton;
import com.kat.xmlprofile.helpers.ApplicationGlobles;
import com.kat.xmlprofile.helpers.Logs;

public class ConsuptionViewPagerAdapter extends FragmentPagerAdapter {

	final int PAGE_COUNT = 4;
	// Tab Titles
	private String tabtitles[] = new String[] { "24 Hours", "24 Days",
			"12 Months", "12 Years" };
	Context context;

	ArrayList<HashMap<String, String>> data;

	public ConsuptionViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public int getCount() {
		Logs.createLog("Total Charts To Load ", String.valueOf(PAGE_COUNT));
		return PAGE_COUNT;

	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public Fragment getItem(int position) {
		switch (position) {

		case 0:
			Chart24Hour chart24Hour = new Chart24Hour();
			return chart24Hour;
		case 1:
			Chart24Days chart24Days = new Chart24Days();
			return chart24Days;
		case 2:
			Chart12Months chart12Months = new Chart12Months();
			return chart12Months;
		case 3:
			Chart12Years chart12Years = new Chart12Years();
			return chart12Years;
		}
		return null;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return tabtitles[position];
	}

	public static double[] convertToDoubleArray(String[] array) {
		double[] tempArray = new double[array.length];
		for (int i = 0; i < array.length; i++) {
			if (Double.parseDouble(array[i]) < 0.01) {
				tempArray[i] = MathHelper.NULL_VALUE;
			} else {
				tempArray[i] = Double.parseDouble(array[i]);
			}
		}
		return tempArray;
	}

	static public class Chart24Hour extends Fragment {
		private BarChart mChart;
		// private Switch mySwitch;
		public Boolean Islive = false;
		BarDataSet set1;
		public static final int TEXT_SIZE_XHDPI = 15;
		public static final int TEXT_SIZE_HDPI = 12;
		public static final int TEXT_SIZE_MDPI = 11;
		public static final int TEXT_SIZE_LDPI = 10;
		Date dt = new Date();
		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
		ArrayList<String> xVals = new ArrayList<String>();

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {
			yVals1.clear();
			xVals.clear();

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mChart.getLegend().setEnabled(false);
			mChart.setTouchEnabled(false);
			// mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			//
			// mySwitch.setChecked(true);

			mChart.animateY(1200);
			mChart.setPinchZoom(false);
			mChart.setScaleEnabled(false);
			mChart.setDoubleTapToZoomEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);
			mChart.getAxisRight().setTextColor(Color.WHITE);
			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			// Code For x Values Roling Charts Starts Here

			// Now fill the xVals array with the correct x-labels
			// current hour to the most right, and oldest hour to the most left
			int currentHour = dt.getHours();

			ArrayList<String> xValsReversed = new ArrayList<String>();

			for (int i = 0; i < 24; i++) {
				xValsReversed.add(String.valueOf(currentHour));
				currentHour--;

				if (currentHour < 0) {
					currentHour = 23;
				}
			}

			xValsReversed.trimToSize();
			// Now reverse the array so it has oldest most left and newest most
			// right
			for (int i = xValsReversed.size() - 1; i >= 0; i--) {
				xVals.add(xValsReversed.get(i));

			}

			// Now add the corresponding y values
			int lastValue = 0;
			for (int i = 0; i < xVals.size(); i++) {
				yVals1.add(new BarEntry(new float[] {
						Float.valueOf(ConsuptionActivity.totalHourArray[Integer
								.valueOf(xVals.get(i))]),
						Float.valueOf(ConsuptionActivity.dhwHourArray[Integer
								.valueOf(xVals.get(i))]) }, Integer.valueOf(i)));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

			mChart.invalidate();

			// mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
			// {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton buttonView,
			// boolean isChecked) {
			//
			// if (isChecked) {
			//
			// Log.e("isChecked:", "true");
			//
			// loadLimitLines();
			// set1.setValueTextColor(Color.WHITE);
			// mChart.invalidate();
			//
			// } else {
			// mChart.getAxisLeft().getLimitLines().clear();
			// set1.setValueTextColor(Color.TRANSPARENT);
			// mChart.invalidate();
			// Log.e("isChecked:", "false");
			//
			// }
			//
			// }
			// });

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivity.totalHourArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 24;

			Logs.createLog("Average Heat Production 24Hour  : ",
					String.valueOf(heatAverage));

			LimitLine llHeatAxis = new LimitLine(heatAverage, "");
			llHeatAxis.setLineWidth(2f);

			llHeatAxis.setLineColor(getResources().getColor(R.color.yellow));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			final YAxis leftAxis = mChart.getAxisLeft();

			// Conditional Checkpoint if llHeatAxis>0.01 then display line only
			// unless no display will be there

			if (heatAverage > 0.1) {

				leftAxis.addLimitLine(llHeatAxis);
			}

			String[] splittedHotWaterValues = ConsuptionActivity.dhwHourArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;

			Logs.createLog("Average Hot Water Production 24 hours  : ",
					String.valueOf(heatHotWaterAverage));

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage, "");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.WHITE);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();

			if (heatHotWaterAverage > 0.1) {

				leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			}

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

		}

	}

	static public class Chart24Days extends Fragment {
		private BarChart mChart;
		// private Switch mySwitch;
		BarDataSet set1;

		final Calendar curreentMonth = Calendar.getInstance();
		ArrayList<String> xVals = new ArrayList<String>();
		ArrayList<String> dateVals = new ArrayList<String>();
		ArrayList<String> dateVals1 = new ArrayList<String>();

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {
			xVals.clear();
			dateVals.clear();
			dateVals1.clear();

			mChart = (BarChart) getView().findViewById(R.id.chart1);

			// mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			// mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setPinchZoom(false);
			mChart.setScaleEnabled(false);
			mChart.setDoubleTapToZoomEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

				@Override
				public void onValueSelected(Entry e, int dataSetIndex,
						Highlight h) {
					// TODO Auto-generated method stub
//					Toast.makeText(getActivity(),
//							String.valueOf(xVals.get(dataSetIndex)), 1000)
//							.show();
					
					
					Toast.makeText(getActivity(),
						String.valueOf(ApplicationGlobles.getdateFromString(dateVals1.get(h.getXIndex())) ), 1000)
							.show();
					Logs.createLog(String.valueOf( h.getXIndex()));

				}
 
				@Override
				public void onNothingSelected() {

				}
			});

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);

			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);
			mChart.getAxisRight().setTextColor(Color.WHITE);

			int currentDay = curreentMonth.get(Calendar.DAY_OF_MONTH);

			int CurrentMonth = curreentMonth.get(Calendar.MONTH);
			CurrentMonth=CurrentMonth+1;
			
			int currrentYear=curreentMonth.get(Calendar.YEAR);
			ArrayList<String> xValsReversed = new ArrayList<String>();

			for (int i = 0; i < 24; i++) {
				xValsReversed.add(String.valueOf(currentDay ));
				dateVals.add(String.valueOf(currentDay+ "-"
						+ CurrentMonth+"-"+currrentYear));

				currentDay--;
				if (currentDay == 0) {

					Calendar LastMonthCalender = Calendar.getInstance();
					// add -1 month to current month
					LastMonthCalender.add(Calendar.MONTH, -1);

					currentDay = curreentMonth
							.getActualMaximum(LastMonthCalender.DAY_OF_MONTH) - 1;

					CurrentMonth = LastMonthCalender.get(Calendar.MONTH);
					CurrentMonth=CurrentMonth+1;
					
					currrentYear = LastMonthCalender.get(Calendar.YEAR);
					

				}
			}

			dateVals.trimToSize();

			xValsReversed.trimToSize();
			// Now reverse the array so it has oldest most left and newest most
			// right
			for (int i = xValsReversed.size() - 1; i >= 0; i--) {
				xVals.add(xValsReversed.get(i));

				dateVals1.add(String.valueOf(dateVals.get(i)));
			}
			dateVals1.trimToSize();

			xVals.trimToSize();

			// int lastValue = curreentMonth
			// .getActualMaximum(Calendar.DAY_OF_MONTH - 1)
			// - Integer.valueOf(xVals.get(0));

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

			for (int i = 0; i < xVals.size(); i++) {
				yVals1.add(new BarEntry(new float[] {
						Float.valueOf(ConsuptionActivity.totalDaysArray[Integer
								.valueOf(xVals.get(i)) - 1]),
						Float.valueOf(ConsuptionActivity.dhwDaysArray[Integer
								.valueOf(xVals.get(i)) - 1]) }, Integer
						.valueOf(i)));
			}
			yVals1.trimToSize();
			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

			mChart.invalidate();

			// mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
			// {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton buttonView,
			// boolean isChecked) {
			//
			// if (isChecked) {
			//
			// Log.e("isChecked:", "true");
			//
			// loadLimitLines();
			// set1.setValueTextColor(Color.WHITE);
			// mChart.invalidate();
			//
			// } else {
			// mChart.getAxisLeft().getLimitLines().clear();
			// set1.setValueTextColor(Color.TRANSPARENT);
			// mChart.invalidate();
			// Log.e("isChecked:", "false");
			//
			// }
			//
			// }
			// });

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivity.totalDaysArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 24;
			Logs.createLog("Average Heat Production 24 Days  : ",
					String.valueOf(heatAverage));

			LimitLine llHeatAxis = new LimitLine(heatAverage, "");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.yellow));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			final YAxis leftAxis = mChart.getAxisLeft();

			// Conditional Checkpoint if llHeatAxis>0.01 then display line only
			// unless no display will be there

			if (heatAverage > 0.1) {

				leftAxis.addLimitLine(llHeatAxis);
			}

			String[] splittedHotWaterValues = ConsuptionActivity.dhwDaysArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {

				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;

			Logs.createLog("Average Hot Water Production 24 Days  : ",
					String.valueOf(heatHotWaterAverage));

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage, "");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.WHITE);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			if (heatHotWaterAverage > 0.1) {

				leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			}
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

		}

	}

	static public class Chart12Months extends Fragment {
		private BarChart mChart;
		// private Switch mySwitch;
		BarDataSet set1;

		Calendar calendar = Calendar.getInstance();

		DateFormatSymbols symbols = new DateFormatSymbols(Locale.US);
		String[] monthNames = symbols.getMonths();

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mChart.setTouchEnabled(false);
			// mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			// mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setPinchZoom(false);
			mChart.setScaleEnabled(false);
			mChart.setDoubleTapToZoomEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			mChart.getAxisRight().setTextColor(Color.WHITE);

			ArrayList<String> xValsint = new ArrayList<String>(0);

			// Code For x Values Roling Charts Starts Here
			ArrayList<String> xVals = new ArrayList<String>();
			for (int i = calendar.get(Calendar.MONTH) + 1; i < 12; i++) {
				xVals.add(monthNames[i]);
				xValsint.add(String.valueOf(i));

			}

			for (int i = 0; i < calendar.get(Calendar.MONTH) + 1; i++) {
				xVals.add(monthNames[i]);

			}

			// Code For x Values Roling Charts Starts Ends
			int lastValue = 12 - Integer.valueOf(xValsint.get(0));

			ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

			for (int i = 0; i < calendar.get(Calendar.MONTH) + 1; i++) {
				yVals1.add(new BarEntry(new float[] {
						Float.valueOf(ConsuptionActivity.totalMonthsArray[i]),
						Float.valueOf(ConsuptionActivity.dhwMonthsArray[i]) },
						lastValue + i));
			}

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "Heat Production",
					"Hot Water Production" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

			mChart.invalidate();

			// mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
			// {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton buttonView,
			// boolean isChecked) {
			//
			// if (isChecked) {
			//
			// Log.e("isChecked:", "true");
			//
			// loadLimitLines();
			// set1.setValueTextColor(Color.WHITE);
			// mChart.invalidate();
			//
			// } else {
			// mChart.getAxisLeft().getLimitLines().clear();
			// set1.setValueTextColor(Color.TRANSPARENT);
			// mChart.invalidate();
			// Log.e("isChecked:", "false");
			//
			// }
			//
			// }
			// });

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivity.totalMonthsArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 12;
			Logs.createLog("Average Heat Production 12 Months  : ",
					String.valueOf(heatAverage));

			LimitLine llHeatAxis = new LimitLine(heatAverage, "");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.yellow));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			final YAxis leftAxis = mChart.getAxisLeft();

			// Conditional Checkpoint if llHeatAxis>0.01 then display line only
			// unless no display will be there

			if (heatAverage > 0.1) {

				leftAxis.addLimitLine(llHeatAxis);
			}

			String[] splittedHotWaterValues = ConsuptionActivity.dhwMonthsArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 12;
			Logs.createLog("Average Hot Water Production 12 Months  : ",
					String.valueOf(heatHotWaterAverage));

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage, "");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.WHITE);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			if (heatHotWaterAverage > 0.1) {

				leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			}
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

		}

	}

	static public class Chart12Years extends Fragment {
		private BarChart mChart;
		// private Switch mySwitch;
		BarDataSet set1;

		Calendar calendar = Calendar.getInstance();

		ArrayList<String> xVals = new ArrayList<String>();

		DateFormatSymbols symbols = new DateFormatSymbols(Locale.US);
		String[] monthNames = symbols.getMonths();
		ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(R.layout.mpcommonchart, container,
					false);

			return view;
		}

		@Override
		public void onActivityCreated(@Nullable Bundle savedInstanceState) {

			mChart = (BarChart) getView().findViewById(R.id.chart1);
			mChart.setTouchEnabled(false);
			// mySwitch = (Switch) getView().findViewById(R.id.mySwitch);
			// mySwitch.setChecked(true);
			// attach a listener to check for changes in state

			mChart.animateY(1200);
			mChart.setPinchZoom(false);
			mChart.setScaleEnabled(false);
			mChart.setDoubleTapToZoomEnabled(false);
			mChart.setDrawGridBackground(false);
			mChart.setDrawBarShadow(false);
			mChart.setDescription("");
			mChart.setDescriptionColor(Color.WHITE);

			mChart.setBackgroundDrawable(getActivity().getResources()
					.getDrawable(R.drawable.bgchart));

			Legend l = mChart.getLegend();
			l.setTextColor(Color.WHITE);
			l.setPosition(LegendPosition.BELOW_CHART_LEFT);

			l.setFormSize(8f);
			l.setFormToTextSpace(4f);
			l.setXEntrySpace(2f);

			mChart.getAxisLeft().setTextColor(Color.WHITE);

			mChart.getXAxis().setPosition(XAxisPosition.BOTTOM);
			mChart.getXAxis().setTextColor(Color.WHITE);

			mChart.getAxisRight().setTextColor(Color.WHITE);

			// Code For x Values Roling Charts Starts Here

			// for (int i = calendar.get(Calendar.YEAR) + 1; i < calendar
			// .get(Calendar.YEAR) + 12; i++) {
			// xVals.add(String.valueOf(i));
			//
			// }
			//
			// for (int i = calendar.get(Calendar.YEAR) - 12; i < calendar
			// .get(Calendar.YEAR) + 1; i++) {
			// xVals.add(String.valueOf(i));
			//
			// }

			int beforeCurrentTwelve = calendar.get(Calendar.YEAR) - 12;

			xVals.clear();
			for (int i = beforeCurrentTwelve; i < calendar.get(Calendar.YEAR) + 1; i++) {
				xVals.add(String.valueOf(i));

			}

			// Code For x Values Roling Charts Starts Ends
			int lastValue = beforeCurrentTwelve;

			yVals1.clear();

			int j = 0;
			for (int i = 0; i < 13; i++) {

				if (beforeCurrentTwelve + i > 2012) {

					yVals1.add(new BarEntry(
							new float[] {
									Float.valueOf(ConsuptionActivity.totalYearsArray[j]),
									Float.valueOf(ConsuptionActivity.dhwYearsArray[j]) },
							i));
					j++;
				}
			}

			// int j = 0;
			// for (int i = 0; i < 12; i++) {
			//
			// if (lastValue + i > 2012) {
			//
			// yVals1.add(new BarEntry(
			// new float[] {
			// Float.valueOf(ConsuptionActivity.totalYearsArray[j]),
			// Float.valueOf(ConsuptionActivity.dhwYearsArray[j]) },
			// i));
			// j++;
			// } else {
			// yVals1.add(new BarEntry(new float[] { Float.valueOf(0),
			// Float.valueOf(0) }, i));
			//
			// }
			// }
			yVals1.trimToSize();

			set1 = new BarDataSet(yVals1, "");

			int[] arrayColors = { getResources().getColor(R.color.green),
					getResources().getColor(R.color.blue) };

			set1.setColors(arrayColors);

			set1.setValueTextColor(Color.WHITE);

			set1.setStackLabels(new String[] { "", "" });

			ArrayList<BarDataSet> dataSets = new ArrayList<BarDataSet>();
			dataSets.add(set1);

			BarData data = new BarData(xVals, dataSets);

			data.setValueFormatter(new MyValueFormatter());

			mChart.setData(data);
			mChart.invalidate();

			// Code For Limit Lines Starts Here

			loadLimitLines();

			// Code For Limit Lines Ends Here

			XAxis bottomAxis = mChart.getXAxis();

			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:
				bottomAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				set1.setValueTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				l.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);
				mChart.setDescriptionTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}

			// mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener()
			// {
			//
			// @Override
			// public void onCheckedChanged(CompoundButton buttonView,
			// boolean isChecked) {
			//
			// if (isChecked) {
			//
			// Log.e("isChecked:", "true");
			//
			// loadLimitLines();
			// set1.setValueTextColor(Color.WHITE);
			// mChart.invalidate();
			//
			// } else {
			// mChart.getAxisLeft().getLimitLines().clear();
			// set1.setValueTextColor(Color.TRANSPARENT);
			// mChart.invalidate();
			// Log.e("isChecked:", "false");
			//
			// }
			//
			// }
			// });

			super.onActivityCreated(savedInstanceState);
		}

		public void loadLimitLines() {

			String[] splittedValues = ConsuptionActivity.totalYearsArray;

			float heatAverage = 0;
			for (String value : splittedValues) {
				heatAverage = heatAverage + Float.valueOf(value);
			}

			heatAverage = heatAverage / 12;
			Logs.createLog("Average Heat Production 12 Years  : ",
					String.valueOf(heatAverage));

			LimitLine llHeatAxis = new LimitLine(heatAverage, "");
			llHeatAxis.setLineWidth(2f);
			llHeatAxis.setLineColor(getResources().getColor(R.color.yellow));
			llHeatAxis.enableDashedLine(10f, 10f, 0f);

			llHeatAxis.setTextColor(Color.WHITE);

			final YAxis leftAxis = mChart.getAxisLeft();

			// Conditional Checkpoint if llHeatAxis>0.01 then display line only
			// unless no display will be there

			if (heatAverage > 0.1) {

				leftAxis.addLimitLine(llHeatAxis);
			}

			String[] splittedHotWaterValues = ConsuptionActivity.dhwYearsArray;

			float heatHotWaterAverage = 0;
			for (String values : splittedHotWaterValues) {
				heatHotWaterAverage = heatHotWaterAverage
						+ Float.valueOf(values);
			}

			heatHotWaterAverage = heatHotWaterAverage / 24;
			Logs.createLog("Average Hot Water Production 12 Years  : ",
					String.valueOf(heatHotWaterAverage));

			LimitLine llHotWaterAxis = new LimitLine(heatHotWaterAverage, "");
			llHotWaterAxis.setLineWidth(2f);
			llHotWaterAxis.setLineColor(Color.WHITE);
			llHotWaterAxis.enableDashedLine(10f, 10f, 0f);

			YAxis leftllHotWaterAxisAxis = mChart.getAxisLeft();
			if (heatHotWaterAverage > 0.1) {

				leftllHotWaterAxisAxis.addLimitLine(llHotWaterAxis);
			}
			switch (getResources().getDisplayMetrics().densityDpi) {
			case DisplayMetrics.DENSITY_XHIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_XHDPI);

				break;
			case DisplayMetrics.DENSITY_HIGH:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;

			case DisplayMetrics.DENSITY_TV:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_HDPI);

				break;
			default:

				leftAxis.setTextSize(Chart24Hour.TEXT_SIZE_LDPI);

				break;
			}
		}

	}

}
