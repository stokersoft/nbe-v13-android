package com.kat.udpapp;

import java.util.HashMap;

import pl.polidea.view.ZoomView;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kat.udpapp.DataListDialogFragment.DataDialogListener;
import com.kat.udpapp.helper.LanguageLoaderSingleton;
import com.kat.xmlprofile.helpers.Logs;

public class SystemSettings extends FragmentActivity implements
		OnClickListener, DataDialogListener {

	LinearLayout llBack;
	Button btnSave;
	private ZoomView zoomView;
	LinearLayout main_container;

	TextView temp;
	String tempKey;

	HashMap<String, Integer> selectedValues;

	Context context = SystemSettings.this;
	View v;

	ImageView imgHelpAutoStartApp, ImageViewAppLanguageHelp;

	TextView txtSpinnerAutoStartApp, txtHeaderTop, txtHelp, txtBack,
			txtSpinnerChangelanguage;
	String[] AutoStartApp = { "Yes", "No" };
	String[] LanguageType = { "English", "DK" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		setContentView(R.layout.activity_system_settings);

		llBack = (LinearLayout) findViewById(R.id.llBack);
		btnSave = (Button) findViewById(R.id.btnSave);

		selectedValues = new HashMap<String, Integer>();
	

		llBack.setOnClickListener(SystemSettings.this);
		btnSave.setOnClickListener(SystemSettings.this);

		txtHeaderTop = (TextView) findViewById(R.id.txtHeaderTop);

		txtHeaderTop.setText(LanguageLoaderSingleton
				.getStringFromLanguage("lng_button_system"));

		txtHeaderTop.setText("System Settings");
		txtHelp = (TextView) findViewById(R.id.txtHelp);
		txtBack = (TextView) findViewById(R.id.txtBack);

		// Code For AutoStart App Setting Starts Here
		imgHelpAutoStartApp = (ImageView) findViewById(R.id.imgHelpAutoStartApp);
		imgHelpAutoStartApp.setOnClickListener(this);

		txtSpinnerAutoStartApp = (TextView) findViewById(R.id.txtSpinnerAutoStartApp);
		txtSpinnerAutoStartApp.setOnClickListener(this);

		if (SecurePreferences.getIntPreference(context, "AutoStartApp") == 0) {
			txtSpinnerAutoStartApp.setText("Yes");
		} else {
			txtSpinnerAutoStartApp.setText("No");

		}
		// Code For AutoStart App Setting ends Here

		// Code For AutoStart App Setting Starts Here
		ImageViewAppLanguageHelp = (ImageView) findViewById(R.id.ImageViewAppLanguageHelp);
		ImageViewAppLanguageHelp.setOnClickListener(this);

		txtSpinnerChangelanguage = (TextView) findViewById(R.id.txtSpinnerChangelanguage);
		txtSpinnerChangelanguage.setOnClickListener(this);

		
		
		if (SecurePreferences.getStringPreference(context,
				Constants.prefLanguage).equals(
				Constants.languageNameEnglise)) {
		
			txtSpinnerChangelanguage.setText("English");
		
			}
	else
		
	{
		txtSpinnerChangelanguage.setText("DK");
	}

		// Code For AutoStart App Setting ends Here

	}

	private void showDataListDialog(String[] array) {
		FragmentManager fm = getSupportFragmentManager();
		DataListDialogFragment editNameDialog = new DataListDialogFragment();
		Bundle args = new Bundle();
		args.putStringArray("data", array);
		editNameDialog.setArguments(args);
		editNameDialog.show(fm, "fragment_country_list");
	}

	@Override
	public void onDataSelected(int result) {

		if (tempKey.contains("AutoStartApp")) {
			temp.setText(AutoStartApp[result]);
		}

	

		
		
		if (tempKey.contains("LanguageType")) {
			temp.setText(LanguageType[result]);
		}

		selectedValues.put(tempKey, result);
		temp = null;
		tempKey = "";

	}

	protected void showTip(String tip) {
		final Dialog dialog = new Dialog(SystemSettings.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dailog_tip);
		dialog.setCanceledOnTouchOutside(true);
		dialog.setCancelable(true);
		TextView txtTip = (TextView) dialog.findViewById(R.id.txtTIP);
		txtTip.setText(tip);
		Button btnOK = (Button) dialog.findViewById(R.id.btnOK);

		btnOK.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	@Override
	public void onClick(View v) {
		if (v == llBack) {
			onBackPressed();
		} else if (v == btnSave) {
			Context context = this;

			for (int i = 0; i < 1; i++) {
				if (selectedValues.get("AutoStartApp") != null) {
					SecurePreferences.savePreferences(context, "AutoStartApp",
							selectedValues.get("AutoStartApp"));
				}
			}
			
			
			if(selectedValues.get("LanguageType").equals(0))
				
			{
			
				
				SecurePreferences.savePreferences(context,
						Constants.prefLanguage, Constants.languageNameEnglise);
				Logs.createLog("Loading English Language");
				LanguageLoaderSingleton.reset();
				LanguageLoaderSingleton
						.getInstance(getApplicationContext());
				
			}
			else
			{

				SecurePreferences.savePreferences(context,
						Constants.prefLanguage, Constants.languageNameDK);
				Logs.createLog("Loading Denish Language");
				LanguageLoaderSingleton.reset();
				LanguageLoaderSingleton
						.getInstance(getApplicationContext());
				
			}
			
			

			Toast.makeText(SystemSettings.this, "Saved", Toast.LENGTH_SHORT)
					.show();
		}
		if (v == imgHelpAutoStartApp) {

			showTip("User Can Enable/Disable App After During Device Boot");

		}

		if (v == txtSpinnerAutoStartApp) {

			temp = txtSpinnerAutoStartApp;
			tempKey = "AutoStartApp";

			showDataListDialog(AutoStartApp);

		}

		if (v == ImageViewAppLanguageHelp) {

			showTip("User Can Switch Language Between English And Denish");

		}

		if (v == txtSpinnerChangelanguage) {

			temp = txtSpinnerChangelanguage;
			tempKey = "LanguageType";

			showDataListDialog(LanguageType);

		}

	}

}
