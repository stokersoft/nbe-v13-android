
package com.kat.udpapp.chart;

import java.util.List;

import org.achartengine.model.CategorySeries;
import org.achartengine.model.MultipleCategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import android.graphics.Color;


public abstract class AbstractDemoChart implements IDemoChart {


  public static void setChartSettings(XYMultipleSeriesRenderer renderer, String title, String xTitle,
      String yTitle, double xMin, double xMax, double yMin, double yMax, int axesColor,
      int labelsColor) {
    renderer.setChartTitle(title);
    renderer.setXTitle(xTitle);
    renderer.setYTitle(yTitle);
    renderer.setXAxisMin(xMin);
    renderer.setXAxisMax(xMax);
    renderer.setYAxisMin(yMin);
    renderer.setYAxisMax(yMax);
    renderer.setAxesColor(axesColor);
    renderer.setLabelsColor(labelsColor);
    
    
    
   
    
    renderer.getSeriesRendererAt(1).setGradientEnabled(true);
    renderer.getSeriesRendererAt(1).setGradientStart(0, Color.rgb(0, 50, 0));
    renderer.getSeriesRendererAt(1).setGradientStop(180, Color.rgb(219, 48, 15));
    
    

    renderer.getSeriesRendererAt(0).setGradientEnabled(true);
    renderer.getSeriesRendererAt(0).setGradientStart(0, Color.rgb(219, 48, 15));
    renderer.getSeriesRendererAt(0).setGradientStop(180, Color.rgb(0, 50, 0));
    

    renderer.setShowGrid(true); 

    
    
  }

  protected MultipleCategorySeries buildMultipleCategoryDataset(String title,
      List<String[]> titles, List<double[]> values) {
    MultipleCategorySeries series = new MultipleCategorySeries(title);
    int k = 0;
    for (double[] value : values) {
      series.add(2007 + k + "", titles.get(k), value);
      k++;
    }
    return series;
  }


 
  public static XYMultipleSeriesDataset buildBarDataset(String[] titles, List<double[]> values) {
    XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
    int length = titles.length;
    for (int i = 0; i < length; i++) {
      CategorySeries series = new CategorySeries(titles[i]);
      double[] v = values.get(i);
      int seriesLength = v.length;
      for (int k = 0; k < seriesLength; k++) {
        series.add(v[k]);
      }
      dataset.addSeries(series.toXYSeries());
    }
    return dataset;
  }

 
  public static XYMultipleSeriesRenderer buildBarRenderer(int[] colors) {
    XYMultipleSeriesRenderer renderer = new XYMultipleSeriesRenderer();
  
    int length = colors.length;
    for (int i = 0; i < length; i++) {
      XYSeriesRenderer r = new XYSeriesRenderer();


      
      r.setColor(Color.WHITE);
      r.setAnnotationsTextSize(20);
     
      renderer.addSeriesRenderer(r);
    }
    

    return renderer;
  }
  

}
