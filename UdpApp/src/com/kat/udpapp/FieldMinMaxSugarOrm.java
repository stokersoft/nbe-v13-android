package com.kat.udpapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kat.models.FieldMinMax;
import com.kat.udpapp.helper.DBHelper;
import com.kat.udpapp.helper.FieldMinMaxORM;
import com.orm.SugarRecord;

public class FieldMinMaxSugarOrm extends SugarRecord<FieldMinMaxSugarOrm> {

	String name;
	double min;
	double max;
	double defaultValue;
	double decimals;

	public FieldMinMaxSugarOrm() {
	}

	public FieldMinMaxSugarOrm(String name, double min, double max,
			double defaultValue, double decimals) {
		super();
		this.name = name;
		this.min = min;
		this.max = max;
		this.defaultValue = defaultValue;
		this.decimals = decimals;
	}

	public static void insertSugarOrmMinMax(Context context,
			FieldMinMaxSugarOrm minMax) {

		minMax.save();
	}

	public static List<FieldMinMaxSugarOrm> getSugarOrmMinMax(Context context,
			String name) {

		List<FieldMinMaxSugarOrm> fieldMinMaxSugarOrm = new ArrayList<FieldMinMaxSugarOrm>();

		fieldMinMaxSugarOrm = FieldMinMaxSugarOrm.find(
				FieldMinMaxSugarOrm.class, "name = ?", name);

		return fieldMinMaxSugarOrm;
	}

}
