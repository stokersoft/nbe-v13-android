/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 */
package com.kat.udpapp;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kat.models.Events;
import com.kat.udpapp.exception.ParseException;
import com.kat.udpapp.helper.LanguageLoaderSingleton;
import com.kat.xmlprofile.helpers.Logs;

import com.kat.udpapp.ControllerRequest;

public class ControllerResponseImpl implements ControllerResponse {

	protected byte[] rawData;
	protected byte[] payload;
	protected int functionId;
	protected int statusCode;
	protected InetAddress senderAddress;
	protected long receiveTime;

	 Set<Events>  eventlist = new LinkedHashSet<Events>(0);

	@Override
	public byte[] getPayload() {
		return payload;
	}

	@Override
	public int getFunctionId() {
		return functionId;
	}

	@Override
	public int getStatusCode() {
		return statusCode;
	}

	@Override
	public void setData(byte[] response) throws ParseException {
		int idx = 0;

		rawData = response;

		// Start character is STX
		if (response[idx++] != ControllerResponse.STX) {
			throw new ParseException("No STX start character");
		}

		// Get function id
		String functionIdStr = "";
		functionIdStr += (char) response[idx++];
		functionIdStr += (char) response[idx++];

		functionId = Integer.valueOf(functionIdStr);
		// functionId = 6;
		if (functionId < 0
				|| functionId > (0x30 + ControllerResponse.FUNCTION_ID_MAX)) {
			throw new ParseException("Bad function id");
		}

		// Get status code
		statusCode = response[idx++];
		statusCode = statusCode - 0x30;
		if (statusCode < 0 || statusCode > (ControllerResponse.STATUS_CODE_MAX)) {
			throw new ParseException("Bad status code");
		}

		// Get size of payload
		String payloadStr = "";
		payloadStr += (char) response[idx++];
		payloadStr += (char) response[idx++];
		payloadStr += (char) response[idx++];
		int payloadSize = Integer.valueOf(payloadStr);

		if (payloadSize < 0 || payloadSize > ControllerResponse.PAYLOAD_MAX) {
			throw new ParseException("Bad payload size");
		}

		// Get actual payload
		try {
			payload = Arrays.copyOfRange(response, idx, idx + payloadSize);
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ParseException("Bad payload data", e);
		}
		idx += payloadSize;

		// Get EOT
		if (response[idx] != ControllerResponse.EOT) {
			throw new ParseException("Bad EOT termination character");
		}

	}

	@Override
	public InetAddress getSenderAddress() {
		return senderAddress;
	}

	@Override
	public void setSenderAddress(InetAddress sender) {
		this.senderAddress = sender;
	}

	@Override
	public long getReceiveTime() {
		return receiveTime;
	}

	@Override
	public void setReceiveTime(long time) {
		receiveTime = time;
	}

	@Override
	public byte[] getRawData() {
		return rawData;
	}

	/**
	 * Quick and dirty function to get the data of the discovery response
	 * 
	 * @param payload
	 * @return An array with first element being the ip of the controller and
	 *         the second element being the serial number
	 */
	public String[] getDiscoveryValues() {
		String values[] = new String[2];
		String payloads[] = new String(payload).split(";");
		String[] elements = payloads[1].split("IP=");
		String ip = elements[1];
		elements = payloads[0].split("Serial=");
		String serial = elements[1];
		values[0] = ip;
		values[1] = serial;
		return values;
	}

	/**
	 * Gets multiple read values.
	 * 
	 * @return One or more read values in a map.
	 * @throws ParseException
	 */
	public Map<String, String> getReadValues() throws ParseException {
		if (this.functionId == ControllerRequest.FUNCTION_READ
				&& this.statusCode == 0) {
			HashMap<String, String> map = new HashMap<String, String>();

			try {
				String[] items = new String(this.payload).split(";");
				for (int i = 0; i < items.length; i++) {
					String keyValue = items[i];
					String[] keyValueParts = keyValue.split("=");
					map.put(keyValueParts[0], keyValueParts[1]);
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
			return map;
		} else {
			throw new ParseException(
					"Response is not a read function response, or error code set (errorCode = "
							+ statusCode + ")");
		}
	}

	/**
	 * Gets multiple read values.
	 * 
	 * @return One or more read values in a map.
	 * @throws ParseException
	 */
	public Map<String, String> getOperationValues() throws ParseException {
		if (this.functionId == ControllerRequest.FUNCTION_READ_OPERATION_DATA
				&& this.statusCode == 0) {
			HashMap<String, String> map = new HashMap<String, String>();

			try {
				String[] items = new String(this.payload).split(";");
				for (int i = 0; i < items.length; i++) {
					String keyValue = items[i];
					String[] keyValueParts = keyValue.split("=");
					map.put(keyValueParts[0], keyValueParts[1]);
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
			return map;
		} else {
			throw new ParseException(
					"Response is not a read function response, or error code set (errorCode = "
							+ statusCode + ")");
		}
	}

	public Map<String, String> getConsuptionValues() throws ParseException {
		if (this.functionId == ControllerRequest.FUNCTION_ID_6
				&& this.statusCode == 0) {
			HashMap<String, String> map = new HashMap<String, String>();

			try {
				String[] items = new String(this.payload).split(";");
				for (int i = 0; i < items.length; i++) {
					String keyValue = items[i];
					String[] keyValueParts = keyValue.split("=");
					map.put(keyValueParts[0], keyValueParts[1]);
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
			return map;
		} else {
			throw new ParseException(
					"Response is not a read function response, or error code set (errorCode = "
							+ statusCode + ")");
		}
	}

	/**
	 * Gets a single read value. Just for convenience.
	 * 
	 * @return A single read value.
	 * @throws ParseException
	 */
	public String getSingleReadValue() throws ParseException {
		if (this.functionId == ControllerRequest.FUNCTION_READ) {
			String keyValue[] = new String(payload).split("=");
			return keyValue[1];
		} else {
			throw new ParseException("Response is not a read function response");
		}
	}

	public String toString() {
		String output = "--- Controller response ---" + "\r\n";
		output += "Function id: " + getFunctionId() + "\r\n";
		output += "Response code: " + getStatusCode() + "\r\n";
		output += "Payload: " + new String(getPayload()) + "\r\n";
		output += "Receive time: " + getReceiveTime() + "\r\n";
		return output;
	}

	public Set<Events> getEventValues() throws ParseException {
		eventlist.clear();
		if (this.functionId == ControllerRequest.FUNCTION_READ_EVENT_LOG
				&& this.statusCode == 0) {

			try {
				String[] items = new String(this.payload).split(";");
				for (int i = 0; i < items.length; i++) {
					eventlist.add(getEvent(items[i]));
				
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
		
		} else {
			throw new ParseException(
					"Response is not a event response, or error code set (errorCode = "
							+ statusCode + ")");
		}
		return eventlist;
	}

	public Events getEvent(String eventdata) throws ParseException {
		Events events = new Events();
		String[] values = eventdata.split(",");
		if (values.length < 6) {
			throw new ParseException("Bad event data in event from controller");
		}
		try {

			events = new Events();

			events.setIsHeader(true);

			for (Events _event : eventlist) {
				if (_event.getDate().equals(values[0])) {

					events.setIsHeader(false);
				} else {
					events.setIsHeader(true);
				}
			}

			events.setDate(values[0]);

			events.setTime(values[1]);

			events.setEventType(Integer.valueOf(values[2]));

			if (Integer.valueOf(values[2]) == 0) {
				events.setEventText(GetEventInfoTypeOne(Integer
						.valueOf(values[3])));
				events.setFirstValue(values[4] + "->" + values[5]);
			}

			if (Integer.valueOf(values[2]) == 1) {
				events.setEventText(GetEventInfoTypeTwo(values[4], values[5]));
				events.setFirstValue("");

			}

			if (Integer.valueOf(values[2]) == 2) {
				events.setEventText(GetEventInfoTypeThree(Integer
						.valueOf(values[3])));
				events.setFirstValue("");
			}

			Logs.createLog(
					"Event Details ",
					events.getDate() + " " + events.getTime() + " "
							+ events.getEventText() + " "
							+ events.getFirstValue());

			events.setEventUnit(values[6]);

		} catch (Exception e) {
			throw new ParseException("Bad event data in event from controller",
					e);
		}
		return events;
	}

	@Override
	public List<Integer> getInfoValues() throws ParseException {
		if (this.functionId == ControllerRequest.FUNCTION_INFO_ITEMS
				&& this.statusCode == 0) {
			List<Integer> list = new ArrayList<Integer>();

			try {
				String[] items = new String(this.payload).split(",");
				for (int i = 0; i < items.length; i++) {
					list.add(Integer.valueOf(items[i]));
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
			return list;
		} else {
			throw new ParseException(
					"Response is not a event response, or error code set (errorCode = "
							+ statusCode + ")");
		}
	}

	/**
	 * Gets multiple min max values.
	 * @return One or more read values in a map.
	 * @throws ParseException
	 */
	public Map<String, String> getMinMaxValues() throws ParseException {
		if (this.functionId==ControllerRequest.FUNCTION_READ_MIN_MAX_DEFAULT && this.statusCode==0) {
			HashMap<String,String> map = new HashMap<String,String>();
			
			try {
				String[] items = new String(this.payload).split(";");
				for (int i=0; i<items.length; i++) {
					String keyValue = items[i];
					String[] keyValueParts = keyValue.split("=");
					map.put(keyValueParts[0], keyValueParts[1]);
				}
			} catch (Exception e) {
				throw new ParseException("Key/value parse problem", e);
			}
			return map;
		} else {
			throw new ParseException("Response is not a read function response, or error code set (errorCode = " + statusCode + ")");
		}
	}
	
	
	public String GetEventInfoTypeOne(Integer id) {
		String text = "setup_" + String.valueOf(id);

		text = LanguageLoaderSingleton.getStringFromLanguage(text);
		text = LanguageLoaderSingleton.getStringFromLanguage(text);
		return text;
	}

	public String GetEventInfoTypeTwo(String valueOne, String valueTwo) {
		String text = "";
		String _valueOne = "state_" + valueOne;
		String _valueTwo = "state_" + valueTwo;

		text = LanguageLoaderSingleton.getStringFromLanguage(_valueOne);
		text = text + " -> "
				+ LanguageLoaderSingleton.getStringFromLanguage(_valueTwo);
		return text;
	}

	public String GetEventInfoTypeThree(Integer id) {
		String text = "lng_system_" + String.valueOf(id);

		text = LanguageLoaderSingleton.getStringFromLanguage(text)
				+ " to the user";

		return text;
	}

}
