
package com.kat.udpapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomButtonRegularSans extends Button
{
	public CustomButtonRegularSans(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}

	public CustomButtonRegularSans(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public CustomButtonRegularSans(Context context)
	{
		super(context);
		init();
	}

	
	public void init()
	{
		if (!isInEditMode())
		{
			Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
					"OPENSANS-REGULAR.TTF");
			setTypeface(tf);
		}
	}
}
