package com.kat.udpapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextViewRegularSans extends TextView
{
	public CustomTextViewRegularSans(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		init();
	}

	public CustomTextViewRegularSans(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		init();
	}

	public CustomTextViewRegularSans(Context context)
	{
		super(context);
		init();
	}

	
	public void init()
	{
		if (!isInEditMode())
		{
			Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
					"OPENSANS-REGULAR.TTF");
			setTypeface(tf);
		}
	}
}
