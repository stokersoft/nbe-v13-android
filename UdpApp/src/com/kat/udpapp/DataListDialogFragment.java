package com.kat.udpapp;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DataListDialogFragment extends DialogFragment {

    ListView list;
    String[] data;
    int dataPosition =0;
    
    public interface DataDialogListener {
        void onDataSelected(int result);
        
    }
    
    public DataListDialogFragment() {
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.country_dialog_fragment, container);
        list = (ListView) view.findViewById(R.id.dialoglist);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
              
        data= getArguments().getStringArray("data");
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.row_gallery_list, data);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				dataPosition = position;
				DataDialogListener activity = (DataDialogListener) getActivity();
	            activity.onDataSelected(position);
				DataListDialogFragment.this.dismiss();
			}
		});
        return view;
    }
}