/**
 *   This file is part of NBE Controller Api.
 *
 *   NBE Controller Api is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   NBE Controller Api is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NBE Controller Api.  If not, see <http://www.gnu.org/licenses/>.
 *   
 */

package com.kat.udpapp;

import java.util.HashMap;
import java.util.Map;

import com.kat.udpapp.ControllerRequest;
import com.kat.udpapp.exception.ParseException;


public class ControllerRequestImpl implements ControllerRequest {

	private int functionId;
	private String code;
	private byte[] payload;
	private byte[] rawdata; 
	
	@Override
	public void setFunctionId(int functionId) {
		this.functionId = functionId;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

    @Override
	public void setRawData(byte[] data) {
    	this.rawdata = data;
    }
	
	
	@Override
	public byte[] getRawRequest() {
		
		// If any data is set directly with setRawData() then return that
		if (rawdata!=null) {
			byte[] rawTemp = rawdata;
			rawdata = null;
			return rawTemp;
		}
		
		int idx = 0;
		
		int size = ControllerRequest.HEADER_SIZE + payload.length;
		byte[] request = new byte[size];
		
		// Start character
		request[idx++] = ControllerRequest.STX;
		
		// Function id
		String funcId = String.valueOf(functionId);
		funcId = ("00" + funcId).substring(funcId.length());
		
		request[idx++] = funcId.getBytes()[0];
		request[idx++] = funcId.getBytes()[1];
		
		// Code
		System.arraycopy(code.getBytes(),0,request, idx, code.getBytes().length);
		idx+=code.getBytes().length;
		
		// Payload size, turn integer into 3 bytes of ascii with leading zeroes
		String payloadSize = String.valueOf(payload.length);
		payloadSize = ("000" + payloadSize).substring(payloadSize.length());
		System.arraycopy(payloadSize.getBytes(),0,request, idx, payloadSize.length());
		idx+=payloadSize.length();
		
		// Payload
		System.arraycopy(payload,0,request, idx, payload.length);
		idx+=payload.length;
		
		// EOT
		request[idx] = ControllerRequest.EOT; 

		return request;
	}

	/**
	 * @return The raw bytes for a discovery request
	 */
	public byte[] getDiscoverRequest() {
		this.setFunctionId(0);
		this.setCode("0000000000");
		this.setPayload(new String("NBE Discovery").getBytes());
		byte[] pk = this.getRawRequest();
		return pk;
	}
	
	/**
	 * @param password The password of the controller
	 * @param value The value to request, like boiler.temp or boiler.*
	 * @return The raw bytes for a read request, given password and value.
	 */
	public void setReadRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_READ);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}

	/**
	 * @param password The password of the controller
	 * @param value The value to request, like boiler.temp or boiler.*
	 * @return The raw bytes for a read request, given password and value.
	 */
	public void setOperationRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_READ_OPERATION_DATA);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}
	
	public void setConsuptionRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_ID_6);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}

	
	
	/**
	 * @param password The password of the controller
	 * @param name The name of the value to set, like boiler.temp
	 * @param value The value to set
	 */
	public void setSetRequest(String password, String name, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_SET);
		this.setCode(password);
		String payload = name + "=" + value;
		this.setPayload(payload.getBytes());		
	}
	
	public void setEventRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_READ_EVENT_LOG);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}

	/**
	 * @param password The password of the controller
	 * @param value The value to request, like boiler.temp or boiler.*
	 * @return The raw bytes for a read request, given password and value.
	 */
	public void setInfoRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_INFO_ITEMS);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}
	
	/**
	 * @param password The password of the controller
	 * @param value The value to request, like boiler.temp or boiler.*
	 * @return The raw bytes for a read request, given password and value.
	 */
	public void setMinMaxRequest(String password, String value) {
		this.setFunctionId(ControllerRequest.FUNCTION_READ_MIN_MAX_DEFAULT);
		this.setCode(password);
		this.setPayload(value.getBytes());
	}
	
	
}
