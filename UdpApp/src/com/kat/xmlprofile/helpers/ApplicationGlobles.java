package com.kat.xmlprofile.helpers;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class ApplicationGlobles {
	public final static int CAMERA_REQUEST_CODE = 2222;
	public final static int MemCard_REQUEST_CODE = 222;
	public static String NoReadingError="No Reading Available";

	public static boolean isNullOrEmpty(String myString)
    {
         return myString == null || "".equals(myString);
    }
	public static void loadStrictModePolicies()
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
		.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
	}
	public static boolean isConnectingToInternet(Activity CurrentActivity) {
		Boolean Connected=false;
		ConnectivityManager connectivity = (ConnectivityManager) CurrentActivity.getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						Log.e("Network is: ", "Connected");
						Connected= true;
					} 
					else
					{	
					}

		}
		else
		{
			Log.e("Network is: ", "Not Connected");
			
			Toast.makeText(CurrentActivity.getApplicationContext(),
					"Please Check Your  internet connection",
					Toast.LENGTH_LONG).show();
			Connected= false;
			
		}
		return Connected;
		
		
	}
	
	public static boolean compareString(String stringOne,String stringTwo)
    {
		Boolean compareStatus=false;
		String fooString1 = new String(stringOne);
		String fooString2 = new String(stringTwo);
		
	 if(fooString1.equals(fooString2))
	 {
		 compareStatus= true;
	 }
	 else
	 {
		 compareStatus= false;
		 
	 }
	 return compareStatus;
    }
	public static Typeface loadFontAusumn(Context _context)
	{
		
		Typeface fontsusumn = Typeface.createFromAsset(_context.getAssets(),
				"fontawesome-webfont.ttf");
		return fontsusumn;
		
	}
	public static String getTodaysDate()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String TodaysDate = sdf.format(new Date());
		return TodaysDate;
		
	}
	
	public static String BitmapTobase64(Bitmap photo)
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		return encodedImage;
		
	}
	
	
	public static String convertMiliSecondToFormatedElapsedTime(long millis) {

		String hms = String.format(
				"%02d:%02d",

				TimeUnit.MILLISECONDS.toMinutes(millis)
						- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS
								.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis)
						- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
								.toMinutes(millis)));

		return hms;

	}
	
	public static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
	
	public static void hideKeyboard(Activity activity)
	{
		
		activity.	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		
	}
	public static String toTitleCase(String input) {
	    StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}
	public static String getdateFromString(String string){
		  
		  String date = string;
		  String arr[] =date.split("-");
		   
		 
		 
		  
		  String Day=arr[0];
		  
		  String Month=arr[1];
		  String Year=arr[2];
		  
		  DateFormatSymbols dfs = new DateFormatSymbols(); 
		  String[] months = dfs.getMonths();
		  
	
		 

		  
		  string=Month+"/"+Day+"/"+Year;
		  
		
		
		
		  
		  
		  
		  SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		  Date date1 = new Date();
		  String dayOfTheWeek = Day+"/"+Month+"-"+Year+"-"+ sdf.format(date1.parse(string));
		   return dayOfTheWeek;
		  
		  
		 }
	
	
	public static String getDayNameFromDateString(String string){
		  
		  String date = string;
		  String arr[] =date.split("-");
		   
		 
		 
		  
		  String Day=arr[2];
		  
		  String Month=arr[1];
		  String Year=arr[0];
		  
		  DateFormatSymbols dfs = new DateFormatSymbols(); 
		  String[] months = dfs.getMonths();
		  
	
		 

		  

		  
		  
		  string=Month+"/"+Day+"/"+Year;
		  
		
		
		
		
		  
		  
		  
		  SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
		  Date date1 = new Date();
		  String dayOfTheWeek = sdf.format(date1.parse(string));
		   return dayOfTheWeek;
		  
		  
		 }
	
	
	
	

}
