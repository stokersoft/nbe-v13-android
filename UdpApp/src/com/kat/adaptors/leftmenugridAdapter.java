package com.kat.adaptors;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kat.models.Leftmenugriditem;
import com.kat.udpapp.R;

public class leftmenugridAdapter extends BaseAdapter {

	private static final String TAG = leftmenugridAdapter.class
			.getSimpleName();
	ArrayList<Leftmenugriditem> listArray;
	public static Context _context;
	public static Activity _activity;


	public leftmenugridAdapter(ArrayList<Leftmenugriditem> arrayProducts,
			Context context, Activity activity) {
		super();
		this.listArray = arrayProducts;
		this._context = context;
		this._activity = activity;

	

	}

	public int getCount() {
		return listArray.size(); // total number of elements in the list
	}

	public Object getItem(int i) {
		return listArray.get(i); // single item in the list
	}

	public long getItemId(int i) {
		return i; // index number
	}

	public View getView(int index, View view, final ViewGroup parent) {

		if (view == null) {
			LayoutInflater inflater = LayoutInflater.from(parent.getContext());
			view = inflater.inflate(R.layout.gridlefticonsrow, parent, false);

		}

		final Leftmenugriditem adsproductItem = listArray.get(index);
		
		 
		

		
		
		TextView textViewtitle = (TextView) view
				.findViewById(R.id.output1Text);
		
		textViewtitle.setText(adsproductItem.getOutputText());

	
		ImageView productImage = (ImageView) view
				.findViewById(R.id.output1Image);
		
		productImage.setImageResource(adsproductItem.getIconImage());
		productImage.invalidate();
		return view;
	}

}