package com.kat.adaptors;

import java.util.ArrayList;

import com.kat.models.DropDownfieldsetsFields;
import com.kat.udpapp.R;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



public class SpinnerArrayAdaptor extends ArrayAdapter<DropDownfieldsetsFields> {
	private Activity context;
	ArrayList<DropDownfieldsetsFields> data = null;

	public SpinnerArrayAdaptor(Activity context, int resource,
			ArrayList<DropDownfieldsetsFields> data) {
		super(context, resource, data);
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		

		return initView(position, convertView, parent);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		
		return initView(position, convertView, parent);
	}

	private View initView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (row == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.layout_spinner, parent, false); 
			
		}

		DropDownfieldsetsFields item = data.get(position);
				
			
		TextView drinkName = (TextView) row.findViewById(R.id.textViewName);
		drinkName.setText(item.getDropDownfieldsetsFieldstxtid());
	
		return row;
	}
}
