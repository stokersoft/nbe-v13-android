package com.kat.adaptors;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kat.models.Events;
import com.kat.udpapp.R;
import com.kat.xmlprofile.helpers.ApplicationGlobles;

public class EventsAdaptor extends BaseAdapter {



	public List<Events> mData = new ArrayList<Events>();
	public TreeSet<Integer> sectionHeader = new TreeSet<Integer>();

	private LayoutInflater mInflater;

	public EventsAdaptor(Context context) {
		mInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void addItem(int i, Events item) {

		mData.add(i,item);
		notifyDataSetChanged();
	}

	

	public List<Events> getAllData() {

		return mData;

	}


	public void addSectionHeaderItem(final Events item) {
		mData.add(item);

		sectionHeader.add(mData.size() - 1);
		notifyDataSetChanged();
	}

	
	@Override
	public int getCount() {
		return mData.size();
	}

	@Override
	public Events getItem(int position) {
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
	

		if (convertView == null) {
			holder = new ViewHolder();

			convertView = mInflater.inflate(R.layout.eventsdataheader, null);

			holder.imageViewEventImage = (ImageView) convertView
					.findViewById(R.id.imageViewEventImage);

			holder.textViewTime = (TextView) convertView
					.findViewById(R.id.textViewTime);

			holder.TextViewEventname = (TextView) convertView
					.findViewById(R.id.TextViewEventname);

			holder.TextViewEventValues = (TextView) convertView
					.findViewById(R.id.TextViewEventValues);
			holder.layoutBG = (LinearLayout) convertView
					.findViewById(R.id.layoutBG);
			holder.textViewDate = (TextView) convertView
					.findViewById(R.id.textViewDate);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();

		}
		String dd = mData.get(position).getDate().substring(0, 2);
		String mm = mData.get(position).getDate().substring(2, 4);
		String yyyy = mData.get(position).getDate().substring(4, 6);

		String dayName = ApplicationGlobles.getDayNameFromDateString(dd + "-"
				+ mm + "-" + yyyy);

		String hh = mData.get(position).getTime().substring(0, 2);
		String MM = mData.get(position).getTime().substring(2, 4);
		holder.textViewTime.setText(hh + ":" + MM +" "+dayName);

		holder.imageViewEventImage.setImageResource(Events
				.getEventImagesFromImagesID(Integer.valueOf(mData.get(position)
						.getEventType())));
		holder.imageViewEventImage.invalidate();

		holder.TextViewEventname.setText(mData.get(position).getEventText());

		
		holder.TextViewEventValues.setText(mData.get(position).getFirstValue());

	
		
		if(mData.get(position).getIsHeader())
		{
		
	
		holder.textViewDate.setText(dayName + " " + dd + "-" + mm + "-" + yyyy);
		holder.textViewDate.setVisibility(View.VISIBLE);	
		
		}
		else
		{
			holder.textViewDate.setVisibility(View.GONE);
			
		}

		return convertView;
	}


	

	private final class ViewHolder {
		public TextView textViewDate;
		public TextView textViewTime;
		public ImageView imageViewEventImage;
		public TextView TextViewEventname;
		public TextView TextViewEventValues;
		public LinearLayout layoutBG;

	}

}