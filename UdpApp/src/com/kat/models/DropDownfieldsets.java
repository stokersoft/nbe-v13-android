package com.kat.models;

import java.util.ArrayList;

public class DropDownfieldsets {
	
	public String DropDownfieldsetsName;
	public String DropDownfieldsetsType;
	public String getDropDownfieldsetsName() {
		return DropDownfieldsetsName;
	}
	public void setDropDownfieldsetsName(String dropDownfieldsetsName) {
		DropDownfieldsetsName = dropDownfieldsetsName;
	}
	public String getDropDownfieldsetsType() {
		return DropDownfieldsetsType;
	}
	public void setDropDownfieldsetsType(String dropDownfieldsetsType) {
		DropDownfieldsetsType = dropDownfieldsetsType;
	}
	
	public ArrayList<DropDownfieldsetsFields> dropDownfieldsetsFields;
	public ArrayList<DropDownfieldsetsFields> getDropDownfieldsetsFields() {
		return dropDownfieldsetsFields;
	}
	public void setDropDownfieldsetsFields(
			ArrayList<DropDownfieldsetsFields> dropDownfieldsetsFields) {
		this.dropDownfieldsetsFields = dropDownfieldsetsFields;
	}
	

}
