/**
 * Holds min, max and default for a controller setup value.
 * This could be boiler.temp or other setup values
 * 
 * 02/07-2015 - stokersoft
 */
package com.kat.models;

public class FieldMinMax {
	
	private String name;
	private double min;
	private double max;
	private double defaultValue;
	private double decimals;
	
	public FieldMinMax(String name, double min, double max, double defaultValue,
			double decimals) {
		super();
		this.name = name;
		this.min = min;
		this.max = max;
		this.defaultValue = defaultValue;
		this.decimals = decimals;
	}
	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	public double getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(double defaultValue) {
		this.defaultValue = defaultValue;
	}
	public double getDecimals() {
		return decimals;
	}
	public void setDecimals(double decimals) {
		this.decimals = decimals;
	}
	
	
}
