package com.kat.models;

import java.util.ArrayList;

public class BoilerFieldSets {

	public ArrayList<Boilerfieldset> arrayBoilerfieldset=new ArrayList<Boilerfieldset>();

	public ArrayList<Boilerfieldset> getArrayBoilerfieldset() {
		return arrayBoilerfieldset;
	}

	public void setArrayBoilerfieldset(ArrayList<Boilerfieldset> arrayBoilerfieldset) {
		this.arrayBoilerfieldset = arrayBoilerfieldset;
	}
}
