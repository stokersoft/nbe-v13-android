package com.kat.models;

import java.util.ArrayList;

import android.content.Context;

import com.kat.udpapp.R;

public class Leftmenugriditem {
	
	public String outputText;
	public int iconImage;
	
	public static ArrayList<Leftmenugriditem> getallIcons(Context context)
	{
		ArrayList<Leftmenugriditem> leftitems=new ArrayList<Leftmenugriditem>(0);
		
		Leftmenugriditem leftmenugriditem=new Leftmenugriditem();
		
		leftmenugriditem.setIconImage(  R.drawable.output_1_pump_boiler);
		leftmenugriditem.setOutputText("On");
		
		leftitems.add(0, leftmenugriditem);
		
		
		
		
 leftmenugriditem=new Leftmenugriditem();
		
		leftmenugriditem.setIconImage(  R.drawable.output_2_motor_value_house);
		leftmenugriditem.setOutputText("Off");
		
		leftitems.add(1, leftmenugriditem);
		
		
		
		 leftmenugriditem=new Leftmenugriditem();
			
			leftmenugriditem.setIconImage(  R.drawable.output_3_pump_house);
			leftmenugriditem.setOutputText("Off");
			
			leftitems.add(2, leftmenugriditem);
			
			
			
			 leftmenugriditem=new Leftmenugriditem();
				
				leftmenugriditem.setIconImage(  R.drawable.output_4_motor_value_1);
				leftmenugriditem.setOutputText("Off");
				
				leftitems.add(3, leftmenugriditem);
		
				
				
				
				 leftmenugriditem=new Leftmenugriditem();
					
					leftmenugriditem.setIconImage(  R.drawable.output_5_motor_value_2);
					leftmenugriditem.setOutputText("On");
					
					leftitems.add(4, leftmenugriditem);
					
					
					
					 leftmenugriditem=new Leftmenugriditem();
						
						leftmenugriditem.setIconImage(  R.drawable.output_6_ashauger);
						leftmenugriditem.setOutputText("Off");
						
						leftitems.add(5, leftmenugriditem);
						
						
						
						 leftmenugriditem=new Leftmenugriditem();
							
							leftmenugriditem.setIconImage(  R.drawable.output_7_vacuum);
							leftmenugriditem.setOutputText("On");
							
							leftitems.add(6, leftmenugriditem);
			
		
		
		
		return leftitems;
		
		
		
		
		
		
	}

	public String getOutputText() {
		return outputText;
	}

	public void setOutputText(String outputText) {
		this.outputText = outputText;
	}

	public int getIconImage() {
		return iconImage;
	}

	public void setIconImage(int iconImage) {
		this.iconImage = iconImage;
	}

}
