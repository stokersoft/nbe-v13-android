package com.kat.models;

import java.util.ArrayList;

public class BoilerSettingSubMenu {
	
	public String Name;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getTxtId() {
		return TxtId;
	}
	public void setTxtId(String txtId) {
		TxtId = txtId;
	}
	public String TxtId;
	
	
	public ArrayList<BoilerSettingSubMenuFields> boilerSettingSubMenuFields=new ArrayList<BoilerSettingSubMenuFields>(0);
	public ArrayList<BoilerSettingSubMenuFields> getBoilerSettingSubMenuFields() {
		return boilerSettingSubMenuFields;
	}
	public void setBoilerSettingSubMenuFields(
			ArrayList<BoilerSettingSubMenuFields> boilerSettingSubMenuFields) {
		this.boilerSettingSubMenuFields = boilerSettingSubMenuFields;
	}

}
