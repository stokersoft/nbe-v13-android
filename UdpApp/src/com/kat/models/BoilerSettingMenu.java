package com.kat.models;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.content.Context;

import com.kat.udpapp.Constants;
import com.kat.udpapp.R;
import com.kat.udpapp.helper.LanguageLoaderSingleton;

public class BoilerSettingMenu {

	public String VersionMin;

	public String VersionMax;

	public String getVersionMin() {
		return VersionMin;
	}

	public void setVersionMin(String versionMin) {
		VersionMin = versionMin;
	}

	public String getVersionMax() {
		return VersionMax;
	}

	public void setVersionMax(String versionMax) {
		VersionMax = versionMax;
	}

	public ArrayList<BoilerSettingSubMenu> getArrayBoilerSettingSubMenu() {
		return arrayBoilerSettingSubMenu;
	}

	public void setArrayBoilerSettingSubMenu(
			ArrayList<BoilerSettingSubMenu> arrayBoilerSettingSubMenu) {
		this.arrayBoilerSettingSubMenu = arrayBoilerSettingSubMenu;
	}

	public ArrayList<BoilerSettingSubMenu> arrayBoilerSettingSubMenu = new ArrayList<BoilerSettingSubMenu>();

	public static BoilerSettingMenu getboilerSettingsFromXml(Context context) {
		BoilerSettingMenu boilerSettingMenu = new BoilerSettingMenu();
		BufferedReader reader = null;
		ArrayList<String> lists = new ArrayList<String>();
		try {
			InputStream is = context.getResources().getAssets()
					.open("profile.xml");

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);

			Element element = doc.getDocumentElement();
			element.normalize();

			// NodeList nList = doc.getElementsByTagName("submenu");
			NodeList Version = doc.getElementsByTagName("version");
			Node n = Version.item(0);
			Node min = n.getAttributes().getNamedItem("min");
			Node max = n.getAttributes().getNamedItem("max");

			String a = "min:" + min.getNodeValue() + "max:"
					+ max.getNodeValue();

			boilerSettingMenu.setVersionMax(String.valueOf(max.getNodeValue()));

			boilerSettingMenu.setVersionMin(String.valueOf(min.getNodeValue()));

			NodeList nodeList = doc.getElementsByTagName("submenu");

			ArrayList<BoilerSettingSubMenu> arrayBoilerSettingSubMenus = new ArrayList<BoilerSettingSubMenu>();

			for (int j = 0; j < nodeList.getLength(); j++) {
				BoilerSettingSubMenu boilerSettingSubMenu = new BoilerSettingSubMenu();
				Node childNode = nodeList.item(j);

				Node nodeBoilerSettingSubMenuname = childNode.getAttributes()
						.getNamedItem("name");

				Node nodeBoilerSettingSubMenuTxtId = childNode.getAttributes()
						.getNamedItem("txtid");

				boilerSettingSubMenu.setTxtId(LanguageLoaderSingleton
						.getStringFromLanguage(nodeBoilerSettingSubMenuTxtId
								.getNodeValue()));

				if (childNode instanceof Element) {

					Element childElement = (Element) childNode;
					NodeList nodeList1 = childElement
							.getElementsByTagName("field");

					ArrayList<BoilerSettingSubMenuFields> BoilerSettingSubMenuField = new ArrayList<BoilerSettingSubMenuFields>();
					for (int k = 0; k < nodeList1.getLength(); k++) {

						Node childNode1 = nodeList1.item(k);
						BoilerSettingSubMenuFields boilerSettingSubMenuFields = new BoilerSettingSubMenuFields();
						Node FieldsTxtId = childNode1.getAttributes()
								.getNamedItem("txtid");

						Node Fieldshelptxtid = childNode1.getAttributes()
								.getNamedItem("helptxtid");

						Node Fieldstype = childNode1.getAttributes()
								.getNamedItem("type");

						if (Fieldstype.getNodeValue().equals("dropdown")) {

							Node Fieldset = childNode1.getAttributes()
									.getNamedItem("fieldset");

							boilerSettingSubMenuFields.setFieldset(Fieldset
									.getNodeValue());

						} else

						{
							boilerSettingSubMenuFields.setFieldset("");

						}

						Node FieldDegree = childNode1.getAttributes()
								.getNamedItem("unittxt");

						Node FieldUDP = childNode1.getAttributes()
								.getNamedItem("udp");

						boilerSettingSubMenuFields.setFieldudp(FieldUDP
								.getNodeValue());

						boilerSettingSubMenuFields
								.setFieldUnitTxt(LanguageLoaderSingleton
										.getStringFromLanguage(FieldDegree
												.getNodeValue()));

						boilerSettingSubMenuFields
								.setFieldsTxtId(LanguageLoaderSingleton
										.getStringFromLanguage(FieldsTxtId
												.getNodeValue())
										+ " "
										+ boilerSettingSubMenuFields
												.getFieldUnitTxt());

						boilerSettingSubMenuFields
								.setFieldshelptxtid(LanguageLoaderSingleton
										.getStringFromLanguage(Fieldshelptxtid
												.getNodeValue()));

						if (isNullOrEmpty(Fieldstype.getNodeValue())) {
							boilerSettingSubMenuFields.setFieldstype("None");

						} else {
							boilerSettingSubMenuFields.setFieldstype(Fieldstype
									.getNodeValue());

						}

						BoilerSettingSubMenuField.add(k,
								boilerSettingSubMenuFields);

					}
					BoilerSettingSubMenuField.trimToSize();

					boilerSettingSubMenu
							.setBoilerSettingSubMenuFields(BoilerSettingSubMenuField);

					System.out.println(childElement.getNodeName() + " "
							+ childElement.getFirstChild().getNodeValue());
				}
				arrayBoilerSettingSubMenus.add(j, boilerSettingSubMenu);
				arrayBoilerSettingSubMenus.trimToSize();
				boilerSettingMenu
						.setArrayBoilerSettingSubMenu(arrayBoilerSettingSubMenus);

			}

			// code to set filedsets for boilersettings dropdowns starts here

			NodeList fieldsets = doc.getElementsByTagName("fieldset");

			ArrayList<DropDownfieldsets> downfieldsets2 = new ArrayList<DropDownfieldsets>();
			for (int m = 0; m < fieldsets.getLength(); m++)

			{
				DropDownfieldsets downfieldsets = new DropDownfieldsets();

				downfieldsets.setDropDownfieldsetsName(fieldsets.item(m)
						.getAttributes().getNamedItem("name").getNodeValue());

				downfieldsets.setDropDownfieldsetsType(fieldsets.item(m)
						.getAttributes().getNamedItem("type").getNodeValue());
				Node childNode = fieldsets.item(m);
				if (childNode instanceof Element) {

					Element childElement = (Element) childNode;
					NodeList nodeList3 = childElement
							.getElementsByTagName("field");

					ArrayList<DropDownfieldsetsFields> arrayDropDownfieldsetsFields = new ArrayList<DropDownfieldsetsFields>();

					for (int k = 0; k < nodeList3.getLength(); k++) {
						DropDownfieldsetsFields downfieldsetsFields = new DropDownfieldsetsFields();

						Node childNode1 = nodeList3.item(k);

						Node FieldsTxtId = childNode1.getAttributes()
								.getNamedItem("txtid");
						String tempFieldsTxtIds = "";

						downfieldsetsFields
								.setDropDownfieldsetsFieldstxtid(LanguageLoaderSingleton
										.getStringFromLanguage(FieldsTxtId
												.getNodeValue()));

						Node Fieldsvalue = childNode1.getAttributes()
								.getNamedItem("value");

						downfieldsetsFields
								.setDropDownfieldsetsFieldsvalue(Fieldsvalue
										.getNodeValue());
						arrayDropDownfieldsetsFields
								.add(k, downfieldsetsFields);

					}

					downfieldsets
							.setDropDownfieldsetsFields(arrayDropDownfieldsetsFields);

				}

				downfieldsets2.add(m, downfieldsets);

			}
			downfieldsets2.trimToSize();
			boilerSettingMenu.setArrayDropDownfieldsets(downfieldsets2);

		} catch (Exception e) { 
			e.printStackTrace();
		}
		return boilerSettingMenu;
	}

	public static boolean isNullOrEmpty(String myString) {
		return myString == null || "".equals(myString);
	}

	public ArrayList<DropDownfieldsets> arrayDropDownfieldsets;

	public ArrayList<DropDownfieldsets> getArrayDropDownfieldsets() {
		return arrayDropDownfieldsets;
	}

	public void setArrayDropDownfieldsets(
			ArrayList<DropDownfieldsets> arrayDropDownfieldsets) {
		this.arrayDropDownfieldsets = arrayDropDownfieldsets;
	}
}
