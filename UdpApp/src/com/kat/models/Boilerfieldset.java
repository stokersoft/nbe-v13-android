package com.kat.models;

import java.util.ArrayList;

public class Boilerfieldset {
	
	public ArrayList<BoilerFieldSetsFieldValues>  arrayBoilerFieldSetsFieldValues=new ArrayList<BoilerFieldSetsFieldValues>();

	public String BoilerfieldsetName;
	public String BoilerfieldsetType;
	public ArrayList<BoilerFieldSetsFieldValues> getArrayBoilerFieldSetsFieldValues() {
		return arrayBoilerFieldSetsFieldValues;
	}
	public void setArrayBoilerFieldSetsFieldValues(
			ArrayList<BoilerFieldSetsFieldValues> arrayBoilerFieldSetsFieldValues) {
		this.arrayBoilerFieldSetsFieldValues = arrayBoilerFieldSetsFieldValues;
	}
	public String getBoilerfieldsetName() {
		return BoilerfieldsetName;
	}
	public void setBoilerfieldsetName(String boilerfieldsetName) {
		BoilerfieldsetName = boilerfieldsetName;
	}
	public String getBoilerfieldsetType() {
		return BoilerfieldsetType;
	}
	public void setBoilerfieldsetType(String boilerfieldsetType) {
		BoilerfieldsetType = boilerfieldsetType;
	}
}
