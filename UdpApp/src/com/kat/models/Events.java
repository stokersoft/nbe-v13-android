package com.kat.models;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kat.udpapp.Constants;
import com.kat.udpapp.ControllerRequest;
import com.kat.udpapp.ControllerRequestImpl;
import com.kat.udpapp.ControllerResponse;
import com.kat.udpapp.NBEControllerClient;
import com.kat.udpapp.R;
import com.kat.udpapp.exception.ParseException;
import com.kat.xmlprofile.helpers.Logs;

public class Events {

	public String Date;
	public String Time;

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getTime() {
		return Time;
	}

	public void setTime(String time) {
		Time = time;
	}

	public Integer getEventType() {
		return EventType;
	}

	public void setEventType(Integer eventType) {
		EventType = eventType;
	}



	public String getFirstValue() {
		return FirstValue;
	}

	public void setFirstValue(String firstValue) {
		FirstValue = firstValue;
	}

	public Boolean getIsHeader() {
		return isHeader;
	}

	public void setIsHeader(Boolean isHeader) {
		this.isHeader = isHeader;
	}

	public Integer EventType;

	public String EventText;

	public String getEventText() {
		return EventText;
	}

	public void setEventText(String eventText) {
		EventText = eventText;
	}

	public String FirstValue;
	public String SecondValue;
	public String EventUnit;

	public String getEventUnit() {
		return EventUnit;
	}

	public void setEventUnit(String eventUnit) {
		EventUnit = eventUnit;
	}

	public String getSecondValue() {
		return SecondValue;
	}

	public void setSecondValue(String secondValue) {
		SecondValue = secondValue;
	}

	public Boolean isHeader = false;

	public static InetAddress controllerAddress;

	public static HashMap<Integer, Integer> HashMapEventImages = new HashMap<Integer, Integer>(
			7);

	public static void SetEventImagesFromImagesID()

	{
		HashMapEventImages.put(0, R.drawable.zero);
		HashMapEventImages.put(2, R.drawable.two);
		HashMapEventImages.put(20, R.drawable.twenty);
		HashMapEventImages.put(3, R.drawable.three);
		HashMapEventImages.put(6, R.drawable.six);
		HashMapEventImages.put(1, R.drawable.one);
		HashMapEventImages.put(4, R.drawable.four);
		HashMapEventImages.put(5, R.drawable.five);

	}

	public static Integer getEventImagesFromImagesID(Integer integer) {

		Integer image = HashMapEventImages.get(integer);
		return image;
	}

	public static  	ArrayList<Events>  getEvents(String DataToLoad)
			throws IOException, ParseException {
		// Date Type Style== 150701= year/MM/dd hhmm ss

		ArrayList<Events>   events = new ArrayList<Events>(0);
		controllerAddress = InetAddress.getByName("185.17.218.11");
		ControllerRequest cr = new ControllerRequestImpl();
		cr.setEventRequest(Constants.password, DataToLoad);
		InetAddress myAddress = InetAddress.getByName(Constants.IpAddress);
		ControllerResponse res2 = NBEControllerClient.getInstance()
				.sendRequest(myAddress, controllerAddress, cr);

		events.addAll(res2.getEventValues());
	
		
		
		
	


		return events;
  
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	public static Comparator<Events> sortedData = new Comparator<Events>() {
		  public int compare(Events one, Events other) {
		   return other .Date.compareTo(
				   one.Date);
		  }
		 };

}
