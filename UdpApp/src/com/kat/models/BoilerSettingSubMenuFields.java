package com.kat.models;

import java.util.ArrayList;

public class BoilerSettingSubMenuFields {

	
	public String FieldsTxtId;
	
	public String Fieldshelptxtid;
	
	public String getFieldsTxtId() {
		return FieldsTxtId;
	}

	public void setFieldsTxtId(String fieldsTxtId) {
		FieldsTxtId = fieldsTxtId;
	}

	public String getFieldshelptxtid() {
		return Fieldshelptxtid;
	}

	public void setFieldshelptxtid(String fieldshelptxtid) {
		Fieldshelptxtid = fieldshelptxtid;
	}

	public String getFieldstype() {
		return Fieldstype;
	}

	public void setFieldstype(String fieldstype) {
		Fieldstype = fieldstype;
	}

	public String Fieldstype;
	public String FieldUnitTxt="";
	public String Fieldudp;
	
	public String Fieldset;
	public String FieldValue;

	public String getFieldValue() {
		return FieldValue;
	}

	public void setFieldValue(String fieldValue) {
		FieldValue = fieldValue;
	}

	public String getFieldset() {
		return Fieldset;
	}

	public void setFieldset(String fieldset) {
		this.Fieldset = fieldset;
	}

	public String getFieldUnitTxt() {
		return FieldUnitTxt;
	}

	public void setFieldUnitTxt(String fieldUnitTxt) {
		FieldUnitTxt = fieldUnitTxt;
	}

	public String getFieldudp() {
		return Fieldudp;
	}

	public void setFieldudp(String fieldudp) {
		Fieldudp = fieldudp;
	}
	
	
	
	}
